---
features:
  secondary:
  - name: Exclude large files using Partial Clone
    available_in: [core, starter, premium, ultimate]
    gitlab_com: true
    documentation_link: https://docs.gitlab.com/ee/topics/git/partial_clone.html
    reporter: jramsay
    stage: create
    categories:
    - Gitaly
    issue_url: https://gitlab.com/gitlab-org/gitaly/-/issues/2553
    description: |
      Storing large binary files in Git is normally discouraged, because every large file added will be downloaded by everyone who clones or fetches changes thereafter. This is slow, if not a complete obstruction when working from a slow or unreliable internet connection.

      In GitLab 13.0, Partial Clone has been enabled for blob size filters, as well as experimentally for tree filters. This allows troublesome large files to be excluded from clones and fetches. When Git encounters a missing file, it will be downloaded on demand. When cloning a project, use the `--filter=blob:none` or `--filer=blob:limit=1m` to exclude blobs completely or by file size. Note, Partial Clone requires at least Git 2.22.0.

      Read more in our recent blog, [How Git Partial Clone lets you fetch only the large file you need](/blog/2020/03/13/partial-clone-for-massive-repositories/). Other filtering specs like `--filter:sparse` are currently disabled due to poor performance.
