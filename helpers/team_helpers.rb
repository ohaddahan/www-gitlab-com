module TeamHelpers
  def direct_team(manager_role:, role_regexp: nil)
    members = Gitlab::Homepage::Team.new.direct_team(manager_role: manager_role, role_regexp: role_regexp)

    partial('includes/team_member_table', locals: { team_members: members })
  end

  # Create a table of team members for a listed department
  def department_team(base_department:, remove_departments: [])
    members = Gitlab::Homepage::Team.new.department_matches(department: base_department)

    remove_departments.each do |removal|
      members -= Gitlab::Homepage::Team.new.department_matches(department: removal)
    end

    partial('includes/team_member_table', locals: { team_members: members })
  end

  # Accepts an array of regexp that will check the role of each member.
  # This allows for finding people in a specific group and those
  # who share their times between groups (e.g. Verify and Verify:Testing)
  def shared_team_members(role_regexps: [])
    members = []

    role_regexps.each do |role_regexp|
      members.push(*Gitlab::Homepage::Team.new.role_matches(role_regexp: role_regexp))
    end

    partial('includes/team_member_table', locals: { team_members: members })
  end

  # Create a paragraph for product group team members
  def product_group(group:)
    members = Gitlab::Homepage::Team.new.department_matches(department: group)
    partial('includes/team_member_list', locals: { team_members: members })
  end

  # Get a list of team members for the group.
  def team_list_from_group(group:)
    Gitlab::Homepage::Team.new.department_matches(department: group, member_type: "person")
  end

  # Create a paragraph for product group team members. Use HTML in case
  # Markdown isn't available in the destination file.
  def team_links_from_group(group:)
    team_list_from_group(group: group)
      .map { |member| "<a href=\"/company/team/##{member.gitlab}\">#{member.name}</a>" }
      .join(", ")
  end

  # Stable counterparts for a team, found by matching roles against
  # role_regexp. Anyone in the direct team reporting to direct_manager_role will
  # be excluded.
  def stable_counterparts(role_regexp:, direct_manager_role:, other_manager_roles: [])
    members =
      Gitlab::Homepage::Team.new.role_matches(role_regexp: role_regexp) -
      Gitlab::Homepage::Team.new.direct_team(manager_role: direct_manager_role)

    other_manager_roles.each do |manager|
      members -= Gitlab::Homepage::Team.new.direct_team(manager_role: manager)
    end

    partial('includes/team_member_table', locals: { team_members: members })
  end

  def hiring_chart(department: nil)
    members = Gitlab::Homepage::Team.new
      .department_matches(department: department)
      .sort_by(&:start_date)

    person_count = vacancy_count = 0
    today_str = Date.today.strftime '%Y-%m-%d'
    people = {}
    people_not_started_yet = []
    vacancies = {}
    vacancies_overdue = []

    members.select { |member| member.type == 'person' }.each do |person|
      person_count += 1
      if person.start_date.to_date > Date.today
        date_str = today_str
        people_not_started_yet.push(person)
      else
        date_str = person.start_date
      end
      people[date_str] = person_count
    end
    people[today_str] = person_count

    vacancy_count = person_count
    members.select { |member| member.type == 'vacancy' }.each do |vacancy|
      vacancy_count += 1
      if date_str = vacancy.start_date.to_date > Date.today
        date_str = vacancy.start_date
      else
        date_str = today_str
        vacancies_overdue.push(vacancy)
      end
      vacancies[date_str] = vacancy_count
    end
    vacancies[today_str] = person_count

    partial('includes/hiring-chart.html.erb', locals: { department: department, people: people, vacancies: vacancies, people_not_started_yet: people_not_started_yet, vacancies_overdue: vacancies_overdue })
  end

  def location_factor_chart(department: nil)
    today_str = Date.today.strftime '%Y-%m-%d'

    # Get the team members in our department
    members = Gitlab::Homepage::Team.new
      .department_matches(department: department)
      .sort_by(&:start_date)

    # Create a time series of people's location factors
    people = {}
    people_with_no_location_factor = []
    members.select { |member| member.type == 'person' }.each do |person|
      start_date = person.start_date.to_date > Date.today ? today_str : person.start_date
      if person.location_factor
        people[start_date] = person.location_factor
      else
        people_with_no_location_factor.push(person)
      end
    end

    # Create a rolling average of people's location factors
    people_rolling_average = {}
    location_factor_total = 0
    count = 1
    people.each do |date, location_factor|
      location_factor_total += location_factor
      people_rolling_average[date] = location_factor_total / count
      count += 1
    end

    # Create a time series of vacancy's location factors
    vacancies = {}
    vacancies_with_no_location_factor = []
    vacancies[people.to_a.last[0]] = people.to_a.last[1] if people.length.positive?
    members.select { |member| member.type == 'vacancy' }.each do |vacancy|
      start_date = vacancy.start_date.to_date < Date.today ? today_str : vacancy.start_date
      if vacancy.location_factor
        vacancies[start_date] = vacancy.location_factor
      else
        vacancies_with_no_location_factor.push(vacancy)
      end
    end

    # Create a rolling average of vacancy's location factors starting with the people rolling average
    vacancy_rolling_average = {}
    first_run = true
    vacancies.each do |date, location_factor|
      location_factor_total += location_factor
      vacancy_rolling_average[date] = if first_run && people_rolling_average.count.positive?
                                        people_rolling_average.to_a.last[1]
                                      else
                                        location_factor_total / count
                                      end
      count += 1
      first_run = false
    end

    # Render the template with data
    locals = {
      department: department,
      people: people,
      people_rolling_average: people_rolling_average,
      people_with_no_location_factor: people_with_no_location_factor,
      vacancies: vacancies,
      vacancy_rolling_average: vacancy_rolling_average,
      vacancies_with_no_location_factor: vacancies_with_no_location_factor
    }
    partial('includes/location-factor-chart.html.erb', locals: locals)
  end

  def department_member_and_vacancy_count(department, counts)
    vacancies = counts[:vacancies]
    count_string = counts[:members].to_s
    count_string << " + #{vacancies} #{'vacancy'.pluralize(vacancies)}" if vacancies.positive?

    "#{department} (#{count_string})"
  end

  def link_to_team_member(gitlab_handle)
    team_member = Gitlab::Homepage::Team::Member.all!.find { |person| person.anchor == gitlab_handle }
    link_to team_member.name, "/company/team/##{team_member.gitlab}" if team_member
  end
end
