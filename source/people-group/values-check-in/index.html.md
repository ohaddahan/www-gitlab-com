---
layout: handbook-page-toc
title: "Values Check In"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Values Check In

GitLab's [values](https://about.gitlab.com/handbook/values/) are a crucial part of our [competencies list](https://about.gitlab.com/handbook/competencies/#list) and are considered essential for team members to learn and live out, irrespective of role or team. To ensure all team members are living out and practicing GitLab's values, we have implemented a Values Check-In survey for new team members and their managers after 8-10 weeks with the company. The purpose of the check-in is to ensure alignment from a performance perspective and understanding of expectations with regard to the values.

The aim of this check-in is to ensure continuous improvment through analysis of trends and feedback, with a positive side-effect that will attribute to great retention and values alignment. 

## Process

We will take [probation periods](https://about.gitlab.com/handbook/contracts/#probation-period) into account when considering the timing of the check in. Team members and managers will have ~_2 weeks_ to complete the survey once it is received. It is essential that the survey is completely in a timely fashion, especially for countries that have a probation period implemented. 

| ***Probation Period Length***           | ***Survey Sent***  | ***Survey Completion Deadline*** |
|-----------------------------------|-----------------|-----------------|
| 1 month | 8 weeks* | 10 weeks*
| 3 months | 6 weeks | 8 weeks
| 6 months | 8 weeks  | 10 weeks
| _No probationary period_ | 8 weeks  | 10 weeks

_*Considering that team members are still [onboarding](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#overview) during their first 30 days with GitLab, team members in countries that follow a 1-month probation period will not have their Values Check-In until after 8 weeks at the company._

### People Experience Process

#### Preparing the Google Sheet
1. Pull a report from BambooHR with the following fields
    - First Name
    - Last Name 
    - Country 
    - Hire Date
    - Reporting to
    - Work Email 
2. Click *More* then *export report as... CSV*
3. Open Google Sheets, then a blank spreadsheet.
4. Click File, Import, Upload, then upload and select the BambooHR CSV file.
5. Filter by *Hire Date* - Newest to Oldest or Z-A
6. Put a blank Column to the right of Hire Date
    - Name this Column *Values Check-In Due Date*
7. To get the due date here, use the formula - Hire date + 70 ie. (=cell+70) the 70 here represents 10 weeks (70 days). 
8. Filter the country listing by
    - China 
    - Hungary
    - Brazil 
    - France 
9. Change the Values Check-In due date for these countries to 56 (8 weeks). Reason here is that these countries have a 3 Month probation and all others have 6 months or not applicable. 
10. Add a column to the right of *Values Check-In Due date* and name *Date Email to be sent*
11. In this column populate with = Values check-in due date - 14, this gives us the date that we should send and the team member 14 days to complete.

#### Sending the Emails
1. Use the info from the column *Date Email to be sent* to determine who to send the email to
2. Send this [email](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/issue_templates/team_member_values_check_in.md) to the team members 
3. Forward the above email to the team members managers with this [email](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/issue_templates/manager_values_check_in.md) 
4. Track responses on the Google form & prompt completion - this is important for those who are on probationary periods. 


### Surveys

- [Team Member](https://docs.google.com/forms/d/e/1FAIpQLSd71MxvRaBjhaxSiFW0qo0blULu9jQ0ypkU7zPEU3p-IimpIQ/viewform) 
- [Manager](https://docs.google.com/forms/d/e/1FAIpQLSfo1OVq-sg2mGu19Nd_fylegKe0068CWfIFN9B8ILjZzlPqow/viewform) 



