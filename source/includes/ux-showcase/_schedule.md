[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Stage groups    |         |                 |             | Host      |
|------------|-----------------|---------|-----------------|-------------|-----------|
| 2020-04-15 | Create          | Release | Growth          | Ecosystem   | Jacki     |
| 2020-04-29 | Secure & Defend | Monitor | Package         | Verify      | Justin    |
| 2020-05-13 | Plan            | Manage  | Secure & Defend | Configure   | Taurie    |
| 2020-05-27 | Create          | Release | Growth          | Enablement  | Mike Long |
| 2020-06-10 | Secure & Defend | Monitor | Package         | Verify      | Marcel    |
| 2020-06-24 | Plan            | Manage  | Secure & Defend | Foundations | Valerie   |
| 2020-07-08 | Create          | Release | Growth          | Ecosystem   | Justin    |
| 2020-07-22 | Secure & Defend | Monitor | Package         | Verify      | Jacki     |
| 2020-08-05 | Plan            | Manage  | Secure & Defend | Configure   | Nadia     |
| 2020-08-19 | Create          | Release | Growth          | Enablement  | Taurie    |
| 2020-09-02 | Secure & Defend | Monitor | Package         | Verify      | Mike Long |
| 2020-09-16 | Plan            | Manage  | Secure & Defend | Foundations | Marcel    |
| 2020-09-30 | Create          | Release | Growth          | Ecosystem   | Valerie   |
| 2020-10-14 | Secure & Defend | Monitor | Package         | Verify      | Justin    |
| 2020-10-28 | Plan            | Manage  | Secure & Defend | Configure   | Jacki     |
| 2020-11-11 | Create          | Release | Growth          | Enablement  | Nadia     |