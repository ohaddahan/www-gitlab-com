---
layout: markdown_page
title: "Category Direction - Release Evidence"
---

- TOC
{:toc}

## Release Evidence

Release Evidence is about addressing the demand of the business to understand what is changing in your software. Our focus is on supporting the variety of controls and automation (security, compliance, or otherwise) to ensure your releases are managed in an auditable and trackable way. 

The backbone of this category is creating a single artifact for our users to furnish during an audit or compliance process. The strong integration across GitLab enables the creation of an auditable chain of custody for assets, commits, issues, including satisfactorily meeting quality and security gates. Table stakes for enterprise-grade governance includes traceability of automated actions alongside the gathering of appropriate approvals throughout the release process. Our intention is to streamline the experience of preparing for an audit or compliance review as an organic byproduct of using GitLab.

Release Evidence is complemented by the tangential category within Release of [Secrets Management](/direction/release/secrets_management). Also related is [Requirements Management](/direction/plan/requirements_management) from the [Plan](/direction/plan/) stage.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARelease%20Governance)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

## What's Next & Why

Now that we have delivered the snapshots for comparison within of Release Evidence via ([gitlab#26019](https://gitlab.com/gitlab-org/gitlab/issues/26019)) and [gitlab#38103](https://gitlab.com/gitlab-org/gitlab/issues/38103) we will be focusing on including more artifacts within evidence. Up next, we are expanding our compliance requirements to include test results within Release Evidence via [gitlab#32773](https://gitlab.com/gitlab-org/gitlab/issues/32773). 

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is "Viable" (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables toward the next maturity target of "Viable":

- [Collect release evidence at moment of release end date](https://gitlab.com/gitlab-org/gitlab/issues/38103) (Complete)
- [Support On Demand Release Evidence Collection](https://gitlab.com/gitlab-org/gitlab/-/issues/199065) (Complete)
- [Include test results in release evidence](https://gitlab.com/gitlab-org/gitlab/issues/32773) (13.0)
- [Highlight release evidence snapshot differences](https://gitlab.com/gitlab-org/gitlab/issues/207852)
- [Expand release audit events](https://gitlab.com/gitlab-org/gitlab/issues/39556)

## Competitive Landscape

A great differentiator for GitLab, is the expansion of the evidence to include test results via [gitlab#32773](https://gitlab.com/gitlab-org/gitlab/issues/32773), security scans, and other artifacts in [gitlab#2207](https://gitlab.com/groups/gitlab-org/-/epics/2207) to be collected as part of a release generation. This will uniquely enable us to be the single source of truth application for the DevOps lifecycle throughout the audit process. 

In today's landscape, "chain of custody" features are ill-defined and not well articulated by our largest competitors. Release Evidence is a strategic feature set that XebiaLabs, Spinnaker, and other CDRA solutions do not readily offer to their users.

## Analyst Landscape

The analysts in this space tend to focus a lot right now on existing, more legacy style deployment workflows, which include better support for validation of approval. The [Compliance Management category](/direction/manage/compliance-management/) will help meet these needs around the granular approval processes within the GitLab workflow.

## Top Customer Success/Sales Issue(s)

Sales and prospects have communicated an interest in expanding more granular permissions within protected environments as solved by [protecting manual jobs](https://docs.gitlab.com/ee/ci/yaml/#protecting-manual-jobs-premium), which will be addressed by Compliance Management category. We are also beginning to get interest in the incorporation of test results within Release Evidence via [gitlab#32773](https://gitlab.com/gitlab-org/gitlab/issues/32773).

## Top Customer Issue(s)

Enabling the upload of assets to releases [gitlab#17838](https://gitlab.com/gitlab-org/gitlab/issues/17838), within Release Evidence satifies many customers' needs to show the rationale of production changes and further strengthens the Release page as a single source of truth.  

We have learned there is demand to support the logging auto-stop actions ([gitlab#36407](https://gitlab.com/gitlab-org/gitlab/issues/36047)) as a part of the release governance story.

Another issue referenced by a handful of customers interested in enterprise-grade governance, includes enforcing the signtaure validation of containers during deployment as captured in our [Binary Authorization MVC](https://gitlab.com/gitlab-org/gitlab/issues/7268). 

## Top Internal Customer Issue(s)

Implementing better practices and expanding the use case documentation of [protecting manual jobs](https://docs.gitlab.com/ee/ci/yaml/#protecting-manual-jobs-premium) will help address many of the internal requests for gating deployments. Our Guided Explorations Project on [Separation of Duties](https://gitlab.com/guided-explorations?filter=separation%20of%20duties%20) illustrates how users can support these needs my leveraging custom CI configuration paths and protected environments. 

## Top Vision Item(s)

Important for this category (though also expansive and includes a few others) is
our epic for [locking down the path to production](https://gitlab.com/groups/gitlab-org/-/epics/762),
which will help us successfully deliver compliance controls within the software delivery pipeline.

The future of Release Evidence includes making the feature easier to work with as an auditor. Features like supporting on-demand evidence creation which will be implemented via [gitlab#199065](https://gitlab.com/gitlab-org/gitlab/-/issues/199065) alongside highlighting the differences between two release evidences in [gitlab#207852](https://gitlab.com/gitlab-org/gitlab/issues/207852), we are making the compliance process surrounding changes to  producition an easy and natural navigation experience in GitLab. 
