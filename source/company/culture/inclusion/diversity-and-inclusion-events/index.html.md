---
layout: markdown_page
title: "Diversity & Inclusion Events"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction

On this page you will be provided an overview of the Diversity & Inclusion events/journey that have taken place or will take place.


##  Summary of Events for 2019:

| Month    | Events                                                         | Outcome / Results         |
|----------|----------------------------------------------------------------|---------------------------|
| Apr | Hired D&I Manager                                              |
| May | D&I Breakout Sessions at Contribute 2019                       | TBC
| Jun | Monthly D&I Initiatives Call                                   | TBC
|          | [Inclusive Language Training](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing)                                  |
|          | [Published GitLab D&I Mission Statement](https://about.gitlab.com/company/culture/inclusion/#diversity--inclusion-mission-at-gitlab)
|          | [Published GitLab's Definition of Diversity & Inclusion](https://about.gitlab.com/company/culture/inclusion/#gitlabs-definition-of-diversity--inclusion)
| Jul | Launched Greenhouse Inclusion Tool
| Aug | GitLab Pride launched                                          | TBC
|          | [GitLab MIT - Minorities in Tech](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/) launched                       | TBC
|          | [GitLab DiversABILITY](https://about.gitlab.com/company/culture/inclusion/erg-gitlab-diversability/) launched                                  | TBC
|          | [GitLab Women](https://about.gitlab.com/company/culture/inclusion/erg-gitlab-women/) launched                                         | TBC
|          | [D&I Advisory Group launched](https://about.gitlab.com/company/culture/inclusion/advisory-group-members/)                                    | TBC
| Sep | [D&I Advisory Group Guidelines](https://about.gitlab.com/company/culture/inclusion/advisory-group-guide/) published
|          | [Published ERG Guidelines](https://about.gitlab.com/company/culture/inclusion/erg-guide/)                                       | TBC
|          | [D&I Framework](https://docs.google.com/presentation/d/1OMgmYc52J02PWacw72ZM_c-R6FYni-BibAhfV514KcQ/edit?usp=sharing)                                                  | TBC
| Oct | [Slack Channels for all ERGs and D&I Advisory Group added](https://about.gitlab.com/company/culture/inclusion/#ergs---employee-resource-groups)       | TBC
| Dec | [Live Learning Inclusion Training](https://www.youtube.com/watch?v=gsQ2OsmgqVM&feature=youtu.be)                              | TBC
|          | [Received D&I Comparably Award](https://about.gitlab.com/blog/2020/01/29/comparable-awards/?utm_medium=social&utm_source=linkedin&utm_campaign=blog)


#  Summary of Events for 2020:

| Month    | Events                                                          | Outcome / Results         |
|----------|-----------------------------------------------------------------|---------------------------|
| Jan | [Live Learning Ally Training](https://www.youtube.com/watch?v=wwZeFjDc4zE&feature=youtu.be)                                     | TBC                                       
|          | D&I Analytics Dashboard - First Iteration                       | TBC                       |
| Feb | Anita Borg becomes an Official Partner                          | TBC
|          | D&I Survey via Culture Amp                                      | TBC                       |
| Mar | Unconscious Bias Training                                       | Being scheduled           |
|          | Working Mother Media Award Submission                           | TBC                       |
| Mar | [Published Building an Inclusive Remote Culture Page](https://about.gitlab.com/company/culture/inclusion/building-diversity-and-inclusion/)           | TBC                        |
| Mar | Published Diversity & Inclusion Events Page                     | TBC                          |
| Apr      | Kickoff Women in Sales Initiatives                              | TBC                       |
| Apr      | Kickoff D&I in Engineering Initiatives                          | TBC                       |
| Apr      | Created the [Parental Leave Toolkit](https://about.gitlab.com/handbook/total-rewards/benefits/parental-leave-buddy/) for Managers and Team Members|
| Apr      | D&I Sessions at Virtual Contribute 2020                         |                           |                  
