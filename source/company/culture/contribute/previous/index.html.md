---
layout: markdown_page
title: "Previous GitLab Contributes (Formerly Summits)"
---

## Summits

- TOC
{:toc}


### Back in the day

Before we evolved to our community event - [GitLab Contribute](https://about.gitlab.com/company/culture/contribute/), getting everybody together was called the GitLab Summits. Summits brought all of our team together every 9-ish months. Below some detatils about those great times we shared.

### Summit in New Orleans, Louisiana, USA

In May 2019, we had 518 team members and 43 significant other’s coming together in New Orleans. We were joined by around a dozen of customers, community members, and investors.

![GitLab Summit - New Orleans - 2019](/images/summits/2019_new-orleans_team.png){: .illustration}*<small>In May 2019, the whole team counted 518 GitLab team-members!</small>*

### Summit in Cape Town, South Africa

At the end of August, in 2018, our team had grown to about 320 people. Over 260 of them came to Cape Town, South Africa. We had over 85 SOs present and were also joined by a handful of customers, our advisors, and investors.

![GitLab Summit - South Africa - 2018](/images/summits/2018_south-africa_team.jpg){: .illustration}*<small>In August 2018, the whole team had grown to 320 GitLab team-members!</small>*

### Summit in Crete, Greece

By October 2017 we had 200 team members and 65 significant others getting together in Greece to enjoy the beautiful islands of Crete and Santorini.

![The GitLab Team in October 2017](/images/summits/2017_greece_team.png){: .illustration}*<small>When October 2017 came around, the whole team already counted 200 GitLab team-members!</small>*


### Summit in Cancun, Mexico

In January 2017, we met in Cancun, Mexico, where roughly 150 team members and 50
significant others flew in from 35 different countries.

![The GitLab team in January 2017](/images/summits/2017_mexico_team.jpg){: .illustration}*<small>In January 2017, the whole team had grown to 150 GitLab team-members!</small>*

<figure class="video_container">
 <iframe src="https://www.youtube.com/embed/XDfTj8iv9qw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<br>

### Summit in Austin, Texas

In May 2016, our team of 85 met up in Austin, TX to see if they were (still) as
awesome as they seemed on Google Hangout.

Here's some footage our team put together to show how much fun we had:

![The GitLab team in May 2016](/images/summits/2016_austin_team.jpg){: .illustration}*<small>Back in May 2016, the whole team was a total of 85 GitLab team-members</small>*

<figure class="video_container">
 <iframe src="https://player.vimeo.com/video/175270564" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<br>

### Summit in Amsterdam, the Netherlands

Here are some impressions from our [second summit](/blog/2015/11/30/gitlab-summit-2015/) in October 2015.

![The GitLab team in October 2015](/images/summits/2015_amsterdam_team.jpg){: .illustration}*<small>In October 2015, the whole team had grown to almost 30 GitLab team-members!</small>*

<figure class="video_container">
 <iframe src="https://www.youtube.com/embed/GJP-3BNyCXw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<br>

### Summit in Novi Sad, Serbia

First summit, less then 10 people attending; highlight was lunch at Marin's mom's home in October 2013.

![GitLab Summit - Serbia - 2013](/images/summits/2013_novi-sad_team.png)

<br>
