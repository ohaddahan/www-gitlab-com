---
layout: handbook-page-toc
title: Support Engineer Responsibilities
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

1. This page is an extension of the [Support Engineer job family](https://about.gitlab.com/job-families/engineering/support-engineer/) page.
1. The **aim** of this page is to **help you know how to carry out your responsibilities as a Support Engineer**.
1. **You're not expected to work on all areas every week**. Each area has a 'frequency' suggestion in parentheses after the heading title. This is to help give you an idea of how often you will work in that area. Your manager will guide you to take part in the right areas at the right times as you gain experience.
1. The '**What does success look like?**' sections help encourage consistent contributions across our global team. They're not 'hard targets' that must be met. They're to help you know how and where to contribute to succeed in your role.
1. **Performance measurement for annual reviews and promotions are separate activities to the core responsibilities described here.**
     1. The People Group is responsible for the [GitLab annual review process](/handbook/people-group/360-feedback/).
     1. Speak to your manager about working towards a promotion and view the [promotion template documents in the Support team drive](https://drive.google.com/drive/folders/1LZ6AkjVv4JW0CiHYTGzyMQdGadyOgDY_) (Internal only).
1. Support Engineering has [team level performance indicators](/handbook/support/#how-we-measure-our-performance). Successfully carrying out the responsibilities on this page will, directly or indirectly, help improve our KPIs. All team-level KPIs are, in turn, related to GitLab's company-level [Objectives and Key Results](https://about.gitlab.com/company/okrs/).

## Support Engineer Areas of Focus

We currently have two main areas of focus for Support Engineers:

1. GitLab.com - helping gitlab.com customers
1. Self-Managed - helping self-managed customers

We also have one area of focus that is more specific:

1. License and Renewals - helping customers with License and Renewals problems
     - Currently there are about four Support Engineers who are focused primarily on this area. These Support Engineers help both .com and self-managed customers.

During onboarding your initial area of focus will be made clear.

When reading this page keep in mind your current area of focus. This will help you locate the correct workflows and roles.

**Note:** We hire [Support Engineers who have a 'Solutions' background, an 'Application' background, or both](https://about.gitlab.com/job-families/engineering/support-engineer/#support-engineering-areas-of-focus). Internally everyone is a 'Support Engineer'. In 2020, we are expanding our internal areas of focus from the two main ones above. This helps solve customer problems more effectively, because you can focus on a narrower range of problem types. For some areas of focus, you have the opportunity to gain expertise in supporting both .com and self-managed customers (*e.g.,* 'License and Renewals'). Support Engineers rotate in and out of areas of focus every few months if desired. Details of this proposal will be available for discussion in an issue during 2020-05.

## 1. Solve Support tickets (Daily)

The core responsibility of a Gitlab Support Engineer is to resolve customer problems by solving support tickets on a daily basis. On average you'll spend more time focusing on this than other areas.

The focus of this responsibility is to solve tickets that are assigned to you.

See [Working On Tickets](/handbook/support/workflows/working-on-tickets.html) for how to do this.

**What does success look like?**

You have the freedom to choose what is best for the customer and GitLab. We value quality over quantity. 

On a weekly basis:

1. [Review at least two tickets with your manager in 1:1 calls](/handbook/support/workflows/working-on-tickets.html#1-weekly-ticket-review). This helps maintain and improve quality (technical, communication and collaboration).
1. [Aim for around 50% to 60% of public replies to be on tickets that are assigned to you](/handbook/support/workflows/working-on-tickets.html#2-public-replies-on-your-assigned-tickets). Assigning tickets helps resolve customer problems quickly and efficiently as well as encouraging learning and collaboration.
1. [Meet the ticket baseline](/handbook/support/workflows/working-on-tickets.html#3-meet-the-ticket-baseline) for solved tickets, public replies, and internal notes for the type of tickets you are working on. This encourages the even distribution of ticket volume across the team.

## 2. Help meet our Service Level Objectives for ticket reply times (Daily)

Our [Support Service Levels](https://about.gitlab.com/support/#gitlab-support-service-levels) mean that we offer round-the-clock support for customers. We work together as a team to meet SLAs, so you will often update tickets *assigned to people in other regions* that are close to breaching SLA.

1. The [First Response Time Hawk](/handbook/support/workflows/meeting-service-level-objectives.html#first-response-time-hawk) makes sure that new tickets are triaged and get a reply as soon as possible.
1. The [SLA Hawk](/handbook/support/workflows/meeting-service-level-objectives.html#sla-hawk) draws attention to tickets that are near SLA breach.

**You should balance your time supporting the SLA hawk with the ['Solve Support Tickets'](#1-solve-support-tickets) responsibility above.**

**What does success look like?**

1. Carrying out the [hawk roles](/handbook/support/workflows/meeting-service-level-objectives.html) when it's your turn in the rotation.
1. Aim for around 40% to 50% of public replies each week to be on tickets that are assigned to someone else. You'll usually make these replies on tickets that are 'near the top of the queue' to help meet our Service Level Objectives.

## 3. Join customer calls (Weekly)

See [Customer Calls](https://about.gitlab.com/handbook/support/workflows/customer_calls.html) for our practices and workflows.

Joining [customer calls](https://about.gitlab.com/support/#phone-support) can be an effective way to speed up ticket resolution. You can choose to schedule a customer call when you believe it's the best next step to help solve a ticket.

**What does success look like?**

We don't currently have a way to make all your contributions to customer calls visible. We're planning to investigate how to do this so you can gauge your contribution and get credit for this valuable work.

The [call summary](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#call-summary) notes you complete as part of the Customer Calls process mean that your work is documented in Zendesk. Highlight any Customer Calls in your 1:1 Meeting Notes with your manager.

## 4. Participate in on-call rotations to coordinate and resolve emergencies (Occasionally)

The following on-call rotations are staffed by Support Engineers:

1. [Self-managed customer emergency on-call](https://about.gitlab.com/handbook/support/on-call/)
2. [GitLab.com Communications Manager on Call (CMOC)](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html)

All Support Engineers participate in one of these rotations - not both, unless you absolutely love being on-call!

**New Team Members:** your Support Engineer Onboarding Issue shows the readiness criteria for joining rotations.

**What does success look like?**

1. Being available when you're scheduled to be on-call (learn how to [swap shifts](/handbook/support/on-call/#swapping-on-call-duty))
1. Responding to alerts within the required time before the alert is escalated
1. Successfully carrying out the on-call communication processes for .com incidents or self-managed emergencies

See the handbook for more details on [Expectations for on-call](/handbook/support/on-call/#expectations-for-on-call).

Be sure to highlight notable incidents in your 1:1 notes doc.

## 5. Collaborate with team members and customers (Daily)

[Collaboration](/handbook/values/#collaboration) is a core value at GitLab. You are encouraged to collaborate with Support team members, other GitLab team members, and customers to help directly and indirectly solve customer problems.

**What does success look like?**

1. Aim for two [pairing sessions](https://gitlab.com/gitlab-com/support/support-pairing) per week. You can see how you're doing on this [pairing summary page](https://gitlab-com.gitlab.io/support/support-pairing/)
1. Ask and answer questions in Slack. (We don't have a way to easily make this visible, but feel free to share things you're proud of with your manager in your 1:1 notes doc.)
1. If you have volunteered to be a [Support Stable Counterpart](/handbook/support/#support-stable-counterparts), collaborate with the group(s) you are assigned to and share knowledge with the Support Team.
1. There are many other ways you can collaborate. Make a note of your collaborations in your 1:1 notes doc.

## 6. Create and update issues for bugs and feature requests (Weekly)

Reducing future customer problems is an important part of being a Support Engineer. [Creating, updating and escalating issues for bugs and feature requests](/handbook/support/workflows/working-with-issues.html) helps achieve this.

**What does success look like?**

1. Aim to create at least two bug reports or feature requests each month. You can see how you're doing using the 'GitLab issues' [activity link](/handbook/support/managers/support-1-1s.html) in your 1:1 notes. Here's an [example link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&author_username=dblessing). The format is `https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&author_username=YOUR_USERNAME`  (replace `YOUR_USERNAME`)

1. GitLab doesn't currently have [a way to find all the comments you've made on Issues](https://gitlab.com/gitlab-org/gitlab/-/issues/28268). Until this feature is available, it's hard to make your contributions to product Issues visible. Instead, be sure that your Zendesk tickets have links to the Issues that you create or update. You can also highlight contributions in your 1:1 notes doc.

## 7. Improve documentation and publicly share knowledge (Weekly)

You are encouraged to [update documentation](/handbook/support/workflows/improving-documentation.html) regularly. This helps prevent ticket creation by improving the information available for customers to use in solving problems without contacting us.

[Creating blog posts](https://about.gitlab.com/handbook/marketing/blog/unfiltered/) and other publicly available knowledge that is accessible by search engines is valuable to help prevent ticket creation.

We [summarize Support team contributions](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues?label_name%5B%5D=support-fix) every week using a bot.

**What does success look like?**

1. Aim for at least two documentation updates every month. You can see how you're doing using the 'Docs updates' [activity link](/handbook/support/managers/support-1-1s.html) in your 1:1 notes. Here's an [example link](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=dblessing&label_name[]=documentation). The format is `https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=YOUR_USERNAME&label_name[]=documentation` (replace `YOUR_USERNAME`)
1. If you publish information in other public places (*e.g.* a blog post), make a note in your 1:1 notes doc.

## 8. Fix GitLab bugs and create features (Occasionally)

Experienced Support Engineers and those familiar with programming are encouraged to [fix GitLab bugs and create features](/handbook/support/#support-fixes).

We [summarize Support team bug fixes and feature requests](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues?label_name%5B%5D=support-fix) every week using a bot.

**What does success look like?**

There's no goal for this area. You can see how you're doing using the 'Support Fix' [activity link](/handbook/support/managers/support-1-1s.html) in your 1:1 notes. Here's an [example link](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=dblessing&label_name[]=support-fix). The format is: `https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=YOUR_USERNAME&label_name[]=support-fix` (replace `YOUR_USERNAME`)


## 9. Improve GitLab and Support processes (Occasionally)

We continuously evolve and improve our processes. You are encouraged to [update the handbook](/handbook/handbook-usage/) and improve Support processes by contributing to [issues in the Support Meta project](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues).

There's a lot going on. It can be overwhelming if you try to contribute to everything. To help focus and find an area you're interested in look at the [Support Team Epics](https://gitlab.com/groups/gitlab-com/support/-/epics/) where issues are grouped into larger initiatives. Epics have a directly responsible individual's (DRI) name in parentheses after the title. Contact them and speak with your manager if you'd like to help out.

**What does success look like?**

There's no goal for this area. You can see how you're doing by using the 'Handbook updates' [activity link](/handbook/support/managers/support-1-1s.html) in your 1:1 notes. Here's an [example link](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=lyle). The format is `https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=YOUR_USERNAME` (replace `YOUR_USERNAME`)

## 10. Develop your skills through learning and training (Weekly)

We are committed to helping you develop your skills through continuous learning. You can complete [courses and bootcamps in the Support Training project](https://gitlab.com/gitlab-com/support/support-training).

The GitLab [Learning and Development team](handbook/people-group/learning-and-development/) provides opportunities for exploration and training.

Support Engineers are also encouraged to complete courses and certification from external providers. Speak with your manager to plan your learning goals.

**What does success look like?**

1. Aim to complete a course or bootcamp every quarter (3 months). You can see how you're doing using the 'Support Training' [activity link](/handbook/support/managers/support-1-1s.html) in your 1:1 notes. Here's an [example link](https://gitlab.com/gitlab-com/support/support-training/-/issues?scope=all&utf8=%E2%9C%93&state=closed&assignee_username[]=cynthia). The format is `https://gitlab.com/gitlab-com/support/support-training/-/issues?scope=all&utf8=%E2%9C%93&state=closed&assignee_username[]=YOUR_USERNAME` (replace `YOUR_USERNAME`)

An important focus for the Support Team in 2020 is to [improve our Learning and Training resources](https://gitlab.com/groups/gitlab-com/support/-/epics/49) to help you have a clear route to improving your skills and a better way to make your expertise visible to the team.

## 11. Improve our learning and training resources (Occasionally)

We encourage Support Team members to update or create [Support Training](https://gitlab.com/gitlab-com/support/support-training) resources.

**What does success look like?**

There's no goal for this area. If you have ideas of materials you'd like to create or update, speak with your manager. You can keep track of updates you've made using the 'Support Training Updates' [activity link](/handbook/support/managers/support-1-1s.html) your 1:1 notes. Here's an [example link](https://gitlab.com/gitlab-com/support/support-training/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=cynthia). The format is `https://gitlab.com/gitlab-com/support/support-training/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=YOUR_USERNAME` (replace `YOUR_USERNAME`)

## 12. Help with hiring (Occasionally)

We encourage the whole team to help with the [Hiring Process](/handbook/support/managers/hiring.html). If you would like to help in this area, discuss with your manager and start an [Interview Training bootcamp](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#)

**What does success look like?**

There's no goal for this area. When you've completed the bootcamp:

1. Promptly respond to requests from recruiting to grade assessments (everyone)
1. Complete technical interviews when scheduled with you (after discussion with manager)

We'd like to make your contributions in this area more visible. Suggestions on how to do this are welcomed.
