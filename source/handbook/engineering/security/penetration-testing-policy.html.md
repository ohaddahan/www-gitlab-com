---
layout: handbook-page-toc
title: "Penetration Testing Policy"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Penetration Testing Policy

A penetration test, (aka pentest or ethical hacking), is a process to identify security vulnerabilities in an application or infrastructure with an attacker's mindset in a safe manner in order to evaluate the security of the system. The test is performed to identify strengths, weaknesses, and vulnerabilities; including the potential for unauthorized parties to gain access to a system's features and data enabling a full risk assessment to be completed.

GitLab performs external, independent penetration testing at least annually with a firm that has a strong reputation within the security industry against [production systems](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#what-is-considered-production) and internally, if it is determined that a significant change has been made to applications or infrastructure.


## Purpose

The purpose of this test is to secure personal, confidential, sensitive data from outsiders, like hackers, who can have unauthorized access to the system. Once the vulnerability is identified it is used to exploit the system to gain access to sensitive information.

A penetration test determines whether or not defensive measures employed on the system are strong enough to prevent security breaches. Penetration test reports also suggest the countermeasures that can be taken to reduce the risk of the system being attacked.

## Why GitLab should perform Penetration Testing

1. To meet the information security compliance at Gitlab and to implement an effective security strategy.
2. Independent testing brings a new perspective which reduces the likelihood of undiscovered errors made by GitLab.
3. Assure customers that their security of data is kept safe from vulnerabilities. We are seeing many customers requesting evidence of our penetration testing as part of contract negotiations. 
4. Penetration testing at regular intervals to protect information systems against security breaches.
5. As a check to ensure that critical, sensitive, and personal data is secured while in-transit.
7. To find security vulnerabilities in an application or infrastructure which reduces the vulnerabilities likely to be discovered through 3rd party reporting which saves GitLab money.
9. To assess the business impact of successful attacks. 

