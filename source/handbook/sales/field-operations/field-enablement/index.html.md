---
layout: handbook-page-toc
title: "Field Enablement"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How to Communicate with Us

Slack: [#field-enablement-team](https://gitlab.slack.com/archives/field-enablement-team)

## Mission

Help customers successfully grow and evolve in their journey with GitLab to achieve positive business outcomes with effective enablement solutions aligned to [Gitlab’s values](/handbook/values/).

## Strategy

<!-- blank line -->
<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQGUQ9g98p31bPZ267en01qJCqgjeX8ZC6GmChBTKz7TV0OEwhFlKbXPgf1YARh5V-lDBegFpu60iTL/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>
<!-- blank line -->

## Key Programs

*  [Sales Onboarding](/handbook/sales/onboarding/)
*  [Command of the Message](/handbook/sales/command-of-the-message)
*  Continuous Learning
    *  [Customer Success Skills Exchange](/handbook/sales/training/customer-success-skills-exchange)
    *  [Sales Enablement Level Up Webcast Series](/handbook/sales/training/sales-enablement-sessions/)
    *  [Sales Training Resources](/handbook/sales/training/)
*  [Field Certification Program](/handbook/sales/training/field-certification)
*  [Sales Manager Best Practices](/handbook/sales/field-operations/field-enablement/sales-manager-best-practices)
*  [Field Flash Newsletter](/handbook/sales/field-communications/field-flash-newsletter/)
*  Sales Events
    *  [Sales Kick Off](/handbook/sales/training/SKO)
    *  [Sales QBRs](/handbook/sales/#quarterly-business-reviews)
    *  [GitLab President's Club](/handbook/sales/club/)

## Handbook-First Approach to GitLab Learning and Development Materials

Chat between David Somers (Director, Field Enablement) and Sid Sijbrandij (CEO)
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/oXTZQpICxeE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Key Discussion Points

*  Our [Mission](/company/strategy/#mission) is that Everyone Can Contribute, and our most important value is [Results](/handbook/values/#results). Like we've extended that to the Handbook, we want to extend it to our Learning Materials.
*  We want to leverage the best of an e-learning platform, with the benefits of reminders, interactivity, and more but make sure the materials we produce are also available to those who aren't using an e-learning platform, while fulfilling [our mission](/company/strategy/#mission). 
*  There are benefits to keeping our e-learning material [handbook-first](/handbook/handbook-usage/#why-handbook-first):
   * Folks who have already completed a formal training through an e-learning platform may want to return to the materials
   * Those who never go through the formal platform may also benefit from the materials
   * The handbook continues to be the SSOT, with the e-learning platform leveraging handbook materials through screenshots, embeds, and more

## Six Critical Questions
Inspired by _The Advantage: Why Organizational Health Trumps Everything Else in Business_ by Patrick Lencioni

1. **Why does the GitLab Field Enablement team exist?**
See our [Mission](/handbook/sales/field-operations/field-enablement/#mission)
1. **How do we behave?**
On our best day, we show up with a positive attitude while demonstrating [GitLab’s values](/handbook/values/) along with the following behaviors to overcome the [Five Dysfunctions](/handbook/values/#five-dysfunctions):
      - **Trust**: Extend trust, actively listen, and assume noble intent; give and receive feedback with respect and solicit feedback often
      - **Embrace Healthy Conflict**: Engage in constructive conflict for the purpose of achieving shared goals & objectives; resolve personal issues, quickly and directly
      - **Commitment**: Support decisions once decisions are made with a GitLab team-first approach
      - **Accountability**: Hold ourselves and each other accountable while encouraging each other & celebrating successes
      - **Results**: Strong drive for results and a focus on the customer; demonstrate passion for continuous learning & improvement
1. **What does the Field Enablement team do?**
   * Define, coordinate, and/or lead the development and delivery of effective enablement solutions (training, technology, knowledge, content, process, and tools) for GitLab sales reps, Customer Success professionals, and partners
   * Lead facilitation of prioritized behavior change in the GitLab field organization
   * Serve as a champion for the field – ensure they are regularly informed about relevant business and organizational updates and have access to enablement materials necessary for day-to-day work 
   * Collaborate cross-functionally with key stakeholders including Sales, Customer Success, Marketing, Sales Ops, Product, and PeopleOps
1. **What does success look like?**
The below is a work in progress as we define success measures across each stage of the customer journey:
   *  **Engage & Educate the Customer**
       * Increase # of rep-sourced opps
       * Accelerate sales cycle time and improve conversion of MQLs to SAOs 
       * Accelerate and improve predictability of new rep ramp time
   *  **Progress the Opportunity & Close the Deal** - 
       * Increase # of closed deals per rep
       * Accelerate sales cycle time and improve conversion of SAOs to Closed/Won deals
       * Increase average sale price (inclusive of improved product mix to sell more Premium/Silver and Ultimate/Gold)
       * Accelerate and improve predictability of new rep ramp time
       * Improve forecasting accuracy
       * Improve win rates
   *  **Retain & Expand**
       * Improve renewal rates (inclusive of up-sell and cross-sell)
       * Accelerate customer time to value
       * Increase breadth of stage adoption
1. **What is most important right now (2HFY20)?**
   1. Field certification program (Sales, CS & Partner audiences)
   1. Sales methodology & process
       * Command of the Message & MEDDPICC operationalization
   1. Functional onboarding training for Sales & Customer Success roles
       * Continued execution & ongoing iteration to accelerate time to productivity for new field team members
   1. Support launch & expansion of new GitLab channel partner program
       * Embrace partner enablement as an extension of GitLab Sales and Customer Success enablement
   1. Standard operating procedures for Sales & Customer Success
       * Sales process documentation
       * Sales enablement to reduce the dependence of SALs & AEs on SAs
   1. Improve learning experience via enhanced L&D tooling 
       * LMS/LXP selection & implementation
   1. Launch sales/field communications strategy
   1. Continuous learning
       * CS Skills Exchange, Sales Enablement Level Up Webcast series
   1. Improve sales asset management (in collaboration with Marketing)
   1. Sales events support (SKO, QBRs) 
1. **Who must do what?**
   *  **[Director, Field Enablement](https://about.gitlab.com/job-families/sales/director-of-field-enablement/)**
      - Create a clear vision for the future that connects the Field Enablement vision & mission to the GitLab and GitLab Sales strategy
      - Oversee direction of current & future portfolio of Field Enablement programs
      - Help the team prioritize, where appropriate
      - Empower, trust, and support team members to develop strategies & tactics to achieve the vision
      - Remove obstacles to achieving that vision
      - Motivate and inspire team members and set the tone for team norms
      - Build & maintain meaningful influence across GitLab
      - Lead global Sales Kick Off (SKO) strategy planning and execution efforts with cross-functional team (including Sales, Corporate Marketing, Product Marketing, and more)
      - Expand professional knowledge and subject matter expertise by attending workshops/training, networking, and reading relevant publications, blogs, books, etc.
   *  **[Program Managers: Enterprise Sales, Commercial Sales, and Technical Sales / Customer Success](https://about.gitlab.com/job-families/sales/program-manager-field-enablement/)**
   *  **[Sales Training Facilitator](https://about.gitlab.com/job-families/sales/sales-training-facilitator-field-enablement/)**
   *  **[Sales Communications Manager](https://about.gitlab.com/job-families/sales/sales-communications-manager/)**

## Field Enablement groups, projects, and labels
   *  **Groups**
      - Use the GitLab.com group for epics that may include issues within and outside the Sales Team group
      - Use the GitLab Sales Team group for epics that may include issues within and outside the Field Operations group
   *  **Projects**
      - Create issues under the “Enablement” project
   *  **Labels**
      - **Team labels**
          - `field enablement` - issue initially created, used in templates, the starting point for any label that involved Field Enablement
          - `FieldOps` - label for issues that we want to expose to the VP of Field Operations; these will often mirror issues with the SCE: priority 1 tag, particularly those relating to OKRs and other prioritized projects by Field Ops leadership
      - **Stakeholder/Customer labels**
          - `FE:CS enablement` - label for Field Enablement issues related to enabling Customer Success (CS) roles
          - `FE:sales enablement` - label for Field Enablement issues related to enabling Sales roles
          - `FE:partner enablement` - label for Field Enablement issues related to enabling Partners
      - **Initiative labels**
          - `continuing education` - label for issues related to continuing education
          - `career dev` - label for issues related to career development
          - `field certification` - label for issues related to https://about.gitlab.com/handbook/sales/training/field-certification/
          - `field communications` - label for items that include work by/with the Field Communications team within Field Enablement
          - `field events` - label for Field Enablement-supported events (e.g. QBRs, SKO, President's Club, etc.) 
          - `force management` - label for issues related to Force Management engagement
             - `vff` - label for Value Framework Feedback
             - `vff::new` - starting point for Value Framework feedback
             - `vff::accepted` = Value Framework feedback that will be actioned on
             - `vff::deferred` = Value Framework feedback that will be deferred until more information is gathered
             - `vff::declined` = Value Framework feedback that is declined (no action will be taken)
          - `onboarding` - label for issues related to onboarding
          - `sales enablement sessions` - label for weekly virtual sales enablement series
          - `sko` - label for issues related to Sales Kick Off
          - `status:plan` - used in conjunction with sales enablement sessions to indicate when a session topic has been prioritized but not yet scheduled
          - `status:scheduled` - used in conjunction with sales enablement sessions to indicate when a session topic has been prioritized and scheduled
      - **Priority labels**
          - `FE:new request` - label for triaging new requests originating outside of the Field Enablement team
          - `FE priority::1` - Home runs (high value to GitLab and high likelihood of success that align to S&CE OKRs) and committed to completion within 90 days. This category will be limited because not everything can be a priority. 
          - `FE priority::2` - Big Bets (high value to GitLab, lower likelihood of success) within 90 days
          - `FE priority::3` - Small wins (lower value to GitLab, high likelihood of success) within 90 days
          - `FE priority::Backlog` - Things in the queue not currently being worked
          - `QBR` - Requests from Sales QBRs
   *  **Field Enablement Issue Boards**
      - [Field Enablement By Initiative Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1191445) 
      - [Field Enablement By Priority Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1644552?&label_name[]=field%20enablement)
      - [Sales Enablement Sessions Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1231617)
      - [Customer Success Skills Exchange Board](https://gitlab.com/gitlab-com/sales-team/cs-skills-exchange/-/boards/1414538)
      - [Sales & CS Onboarding Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1645038)
