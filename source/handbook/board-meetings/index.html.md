---
layout: handbook-page-toc
title: "Board of Directors and Corporate Governance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Corporate Governance Documents

1. [Amended and Restated Bylaws of GitLab Inc. (January 31, 2019)](/handbook/board-meetings/bylaws.html)

## Board Selection Process

### Roles and Responsibilities
* [Manager, Recruiting Operations](/job-families/people-ops/recruiting-operations/) - Responsible for drafting communication to potential Board members. Email communication with Nominating and Corporate Governance Committee when questions arise or guidance is needed.
* [CEO's Staff Executive Business Administrator](/job-families/people-ops/executive-business-administrator/) - DRI for Board and Committee scheduling.
* [CLO](/job-families/legal/chief-legal-officer/) - Nominating and Corporate Governance Committee DRI.
* [Nominating and Corporate Governance Committee](/handbook/board-meetings/#nominating-and-corporate-governance-committee) - Collaborate on Board evolution. Provide recommendations for potential new GitLab BoD members; meet and vet potential new board members; provide feedback on candidates.
* CEO and Selected Members of [E-Group](/handbook/leadership/#e-group) - Meet and vet potential new board members; provide feedback on candidates.

### Interview Process

#### Independent
1. First meeting with the nominating board member;  50-minute meeting.
1. CEO and Board members: 50-minute meetings.
1. GitLab CRO, CFO, CLO and other E-Group members as requested.
1. Any parties as requested by the candidate.

## Board Renewal Process
Assuming GitLab adopts a three class structure, a Director can belong to Class I, Class II or Class III. Some of our Directors will be in Class 1 and will be up for reelection as early as the first year the company is public, while other Directors will not be up for reelection until 2 or 3 years after the company is public. The decision as to which director is in which class is typically made close to when a company goes public. Various factors in making this determination are taken into consideration; for example, some VC Directors may prefer to leave the Board earlier in the public company lifecycle due to the number of commitments or requirements imposed by their respective funds.

## Board Meeting Process

### Timeline

1. The Staff EBA to the CEO is the DRI for scheduling the Board meetings and updating the dates on this page.
1. The Chief of Staff (CoS) is the DRI for assembling the deck and sends a reminder to the E-Group and EBA team 4 weeks in advance of the meeting with the templates for the materials to be created.
1. The CoS reaches out before the meeting to collect questions from the board members and prepares and shares the agenda.
1. The CoS sends another reminder to the E-Group 2 weeks in advance of the meeting.
1. The E-Group has the complete presentations ready 8 days prior to the meeting.
1. Eight days before the meeting, the CoS submits all materials to Legal and Finance for review.
1. Once approved, the CoS distributes the Board materials 7 days before the meeting. All presentations and content should be finalized at this time with no further changes. The Staff EBA to the CEO is responsible for sharing settings on all materials. Materials should be shared with the E-Group, the `board@` alias, Board Observers, and the outside legal firm. Any exceptions must be confirmed by the Staff EBA to the CEO before shipping.
1. The CFO is the DRI for the Administrative deck and will share this with the CLO and Staff EBA to the CEO at least 1 week before the board meeting and then with the Board 4 business days prior to the Board meeting.
1. The day before the meeting, the Staff EBA to the CEO ensures that the Introduction section of the agenda is up to date with the latest attendees.
1. The CoS is responsible for ensuring that the Board meeting runs according to the time allotted.
1. The CLO is responsible for maintaining the minutes of the meeting and submitting for Board approval.
1. The CLO ensures that PDF versions of the materials including presentations, exhibits, appoved minutes, and administrative materials are securely stored on Google Drive labeled with the date of the meeting.

#### Next Meeting's Timeline
This section is updated after every Board Meeting by the CoS for the next Board Meeting

1. Meeting is scheduled for 2020-06-04.
1. 2020-05-07 The CoS sends a reminder to the E-Group and EBA team 4 weeks in advance of the meeting.
1. 2020-05-14 The CoS reaches out before the meeting to collect questions from the board members and prepares and shares the agenda.
1. 2020-05-21 The CoS sends another reminder to the E-Group 2 weeks in advance of the meeting.
1. 2020-05-27 The E-Group has the complete presentation ready 8 days prior to the meeting.
1. 2020-05-27 Eight days before the meeting, the CoS submits all materials to Legal and Finance for review.
1. 2020-05-28 Once approved, the CoS distributes the Board materials 7 days before the meeting. The Staff EBA to the CEO is responsible for sharing settings on all materials. Materials should be shared with the E-Group, the `board@` alias, Board Observers, and the outside legal firm. Any exceptions must be confirmed by the Staff EBA to the CEO before shipping.
1. 2020-06-03 The Staff EBA to the CEO ensures that the Introduction section of the agenda is up to date with the latest attendees.
1. 2020-06-04 Board of Directors Meeting; CoS ensures the meeting runs according to the time allotted.
1. 2020-06-05 The CLO ensures that PDF versions of the materials including presentations, exhibits, approved minutes, and administrative materials are securely stored on Google Drive labeled with the date of the meeting.

### Board Deck

1. The Board deck should follow a specific template and not exceed 10 slides per function, unless the function is scheduled to provide a deep dive.
1. Each E-Group member may include 1 additional slide with their asks of the Board.
1. Slides should convey a high-level overview as opposed to operational updates and should not include embedded links.
1. Put information on public webpages such as [/strategy](/company/strategy/) as much as possible.
1. Sensitive information goes into the Administrative deck the CFO owns.

#### Finance Presentation
The finance slides are as follows:
1. Review past quarter and YTD: Bookings review (growth rates) / new/growth and by sales zone
1. Review past quarter and YTD: P&L vs plan with commentary
1. Review past quarter and YTD: Revenue deep dive
1. Review past quarter and YTD: Gross Margin deep dive
1. Review past quarter and YTD: Cash waterfall
1. Forward looking: Rolling forecast that we will hold company accountable to in next quarter with commentary
1. Forward looking: Cash projection through end of year
1. Forward looking: LR plan adjustments with commentary
1. Forward looking: Balance sheet projection to match the 4Q Forecast

### CEO Video and Memo

The CEO is responsible for submitting both a memo and a video to the BoD in advance of the Board meeting. The memo should be 1.5-2 pages capturing the highlights across the organization for the quarter. The video should be in the style of an earnings call to give a general overview of what is and is not working. The CoS will upload the video to YouTube as an unlisted video and ensure both the video and the memo get distributed to the Board with the other materials.

### Deep Dives

Deep dives are the only functions with greater than 10 slides.
Deep dives are 8-minute presentations followed by Q&A up to the 30 minute allotment.

There will be two deep dives on the agenda for each board meeting:

1. 30 minutes allotted to a single functional area on a rotational basis
1. 30 minutes allotted to a topic(s) of strategic or operational importance to the Company

The functional deep dive rotation schedule is as follows:
* Product management and product strategy (2020-06)
* Finance (2020-09)
* Legal (2020-12)
* Engineering (2021-03)
* Sales (2021-06)
* Marketing (2021-09)
* People (2021-12)

Functional deep dives cover the following:
* Highlights over the last quarter (how we got here)
   * Additional details on each highlight
* Major wins (in case there's anything worth mentioning that is a win, but might not be a highlight)
* Key challenges
   * Additional details on each key challenge
* Upcoming budget
   * Budget overview
   * Intent for budget
   * Themes for investment
* Initiatives and what to expect in the next quarter (peak to year)
* OKRs
   * Past quarter OKRs & Results
   * Next quarter OKRs & Results
* Function KPIs
* Capabilities and org structure

### Agenda

1. We try to avoid presentations during meetings, but there are two presentations at all Board Meetings:
   * 8 minutes for any deep dives
   * 5-10 minutes during the Administrative Session for Board Committee readouts by the Committee Chairperson updating the Board as to the Committee’s activities and highlighting the items requiring Board approval, if any.
1. Prior to distributing the board deck, the CoS groups questions together by function.
1. After the General Session of the Board Meeting, there is then an "Administrative Session" attended by the CFO and CLO and the full Board (along with Observers).
1. At the conclusion of the Administrative Session the CFO and CLO depart and the full Board shall meet for a closed session.

### Location

The board meeting is [all remote](/company/culture/all-remote/) because half remote is a bad experience for remote participants, see [video calls](/handbook/communication/#video-calls) point 10.

## Executive Stable Counterparts

Every GitLab Executive, except the CEO, has a [Stable Counterpart](/handbook/engineering/infrastructure/library/organization/stable-counterparts/).
The Board Member and Exec should meet at least once per quarter.
The goal of codifying Board Member-Exec Stable Counterparts is to help build relationships between Execs and Board Members and maintain clear lines of communication outside of traditional methods, such as Board meetings.
“Can you please give me your SWOT analysis of the market?”, “What are you seeing competitively in the field?”, “What interesting products are getting traction in our market?” are productive examples of questions from Board Members.

| Executive (by start date) | Board Member |
|------------------|--------------|
| [Sid Sijbrandij](/company/team/?department=executive#sytse) | [Godfrey Sullivan](/company/team/?department=board#godfrey-s) |
| [Paul Machle](/company/team/?department=executive#pmachle) | [Karen Blasing](/company/team/?department=board#karen-b) |
| [Mark Pundsack](/company/team/?department=executive#markpundsack) | (Future Board Member) |
| [Eric Johnson](/company/team/?department=executive#edjdev) | (Future Board Member) |
| [Michael McBride](/company/team/?department=executive#mmcb) | [Bruce Armstrong](/company/team/?department=board#bruce-a) |
| [Todd Barr](/company/team/?department=executive#tbarr) | [Sue Bostrom](/company/team/?department=board#sue-bostrom) |
| [Scott Williamson](/company/team/?department=executive#sfwgitlab) | [Matthew Jacobson](/company/team/?department=board#matthewjacobson) |
| [Robin Schulman](/company/team/?department=executive#rschulman) | [David Hornik](/company/team/?department=board#david-hornik) |
| (Future Chief People Officer) | (Future Board Member) |

## Board and Committee Composition

* [Board of Directors Job Description](/job-families/board-of-directors/board_member/)

### Board of Directors

**Members:** Sid Sijbrandij, Bruce Armstrong, Matthew Jacobson, David Hornik, Sue Bostrom, Karen Blasing, Godfrey Sullivan

### Audit Committee

[Audit Committee Charter](/handbook/board-meetings/#audit-committee-charter)

**Chairperson:** Karen Blasing
**Members:** Bruce Armstrong, David Hornik
**Management DRI:** Chief Financial Officer

### Compensation Committee

[Compensation Committee Charter](/handbook/board-meetings/#compensation-committee-charter)


**Chairperson:** Sue Bostrom
**Members:** Bruce Armstrong, Matthew Jacobson
**Management DRI:** Chief People Officer

### [Nominating and Corporate Governance Committee](https://about.gitlab.com/handbook/board-meetings/committees/nomgov/)

[Nominating and Corporate Governance Committee Charter](/handbook/board-meetings/#nominating-and-corporate-governance-committee-charter)

**Chairperson:** Matthew Jacobson
**Members:** Sid Sijbrandij, Sue Bostrom
**Management DRI:** Chief Legal Officer

## Board Meeting Schedule
1. Board of Directors meetings are held quarterly and they are all remote; everyone joins with their own video conference setup from a separate location.
1. Meetings are scheduled on the Thursday 6 weeks after the end of the quarter, assuming availability of the Directors.
1. The 2020/2021 schedule of Board meetings is as follows:
* 2020-03-05
* 2020-06-04
* 2020-09-09
* 2020-12-08
* 2021-03-18
* 2021-06-17
* 2021-09-21
* 2021-12-14

The Staff EBA to the CEO shall ensure that there are separate calendar invites for all attendees within each session, all including exact session start and end times, the appropriate Zoom link, and links to the notes doc, agenda and any supplemental materials:
* Board of Directors Meeting - General Session
   * Duration: 2 hours
   * Attendees: GitLab Board of Directors, E-Group, GitLab team members participating in the deep dive, outside legal firm representatives, Board observers
* Session 2: Administration Session
   * Duration: 25 - 50 minutes, as needed
   * Attendees: GitLab Board of Directors, GitLab CFO and CLO, outside legal firm, Board observers
* Session 3: Closed Session
* Duration: 50 minutes
   * Attendees: GitLab Board of Directors

### Audit Committee Agenda Planner

We review the below topics no less frequently than the following schedule:


#### Management, Accounting and Reporting

|  Topics                                                                               | FY Q1 | FY Q2  |  FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Accounting policies                                                                        |    |  X   |           |  X     |
| Significant estimates and judgements                                 |          |           |     | X|
| New accounting standards – impact and implementation plan            |        X |    X       | X | X      |
| Review of financial Statements                                       |        X   |           |   |       |
| Related party transactions                                           |          |           |   |  X    |
| Treasury                                                             |          |           |   |  X    |
| ERM – Review of financial statement  risk factors                    | X        |           |  |      |
| Insurance coverage update                                            |          |           |   | X   |
| Close process                                                        |        X |            |   |        |
| Stock transactions                                                   |        X |            |   |        |
| Tax audits / Taxes                                                   |          |            |   | X      |
| Public reporting (GAAP and Non-GAAP financials, non-GAAP metrics)   |        X |            |   |        |
| Guidance model                                                       |        X |            |   |        |

#### People Division

| Topics                          | FY Q1  |   FY Q2  |  FY Q3 | FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Global staffing update, succession plan and continuous improvement   |     X    |           |   |       |
| EEO audits                                                           |     X    |           |   |       |
| Payroll                                                              |          |      X    |   |       |
| Compensation and hiring                                              |     X    |           |   |       |

#### Legal, Risk and Compliance

| Topics                          | FY Q1  |   FY Q2  |  FY Q3 | FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Compliance to business conduct (including hotline complaints and code of conduct violations) |    X    |           | |     |
| ERM – Risk assessment updates                              |    X      |     |       |       |
| Regulatory compliance  |          |   |    X  |      |
| Privacy                |          | X |       |      |
| Reg FD  - Fair Disclosure  |     X     |   |      |      |
| Reg G - Governance        |      X   |   |      |      |
| Committee annual assessment  |          | X  |      |      |

#### Security Compliance

| Topics                          | FY Q1  |   FY Q2  |  FY Q3 | FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| ERM – Cyber risk assessment         |     X    |         |    |      |
| Cybersecurity                                    |         |     X      |  |      |
| Application system reviews:<br><br> Tech Stack - for accounting function & GCF handbook<br> 1. Accounting - Netsuite<br> 2. Tax - Avalara<br> 3. Stock - Carta<br> 4. Planning - NA<br>  5. HR - Bamboo HR<br>  6. License provisioning - Zuora<br> 7. Commission    system - Captivate                                      |     X     |           |   |     |
| IT implementation projects and initiatives  |     X    |   |      |      |
| IT security update              |     X    |   |      |      |

#### Internal Audit

| Topics                          | FY Q1 | FY Q2 |  FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Internal audit and global annual plan                           |                 |        |           |   X   |
| Internal audit activity report and annual plan update            |         X         | X         |     X      |   X  |
| SOX - Internal control over financial reporting assessment and deficiencies status update  |    X | X      | X     |  X       |
| Internal controls (pre-Sox)                                    |         |   X     |   |       |
| Internal audit charter review                                     |     X    |        |   |       |
| Fraud Risk assessment                                    |      X    |           |   X  |  |
| Annual assessment of internal audit   |          |       X    |     X    |  |

#### External Audit

| Topics                          | FY Q1 | FY Q2  |   FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Global audit plan and fees/Appoint External Auditor |  | | X |X |
| Year-end audit results and required communications, as applicable |         |   X    |   |    X    |
| Annual assessment of audit firm, engagement team and lead audit partner                      |          | |    X    |       |
| Independence review | | | X| X |
| Audit               |X| |  |   |

#### General

| Topics                          | FY Q1 | FY Q2  |  FY Q3  |  FY Q4 |
|:---------------------------------|:--------:|:---------:|:-------:|:-------:|
| Executive session - as needed  | X | X | X |X |
| Approval of minutes                                        |X  | X     | X        |X      |
| Committee annual assessment                            |          |    X       |      | |
| Closed session as needed         |X |    X | X     |X         |



## Quarterly AMA with the Board
We will have one Board member per quarter conduct an AMA (Ask Me Anything) session with the GitLab team.  AMA’s are 25 minutes and live streamed to GitLab Unfiltered on YouTube.
* [David Hornik's AMA](https://www.youtube.com/watch?v=LwZNbF6_PX0%C2%A0)

## References

1. [AVC post](http://avc.com/2016/02/do-you-want-better-board-meetings-then-work-the-phone/)
1. [AVC comment](http://avc.com/2016/02/do-you-want-better-board-meetings-then-work-the-phone/#comment-2489615046)
1. [Techcrunch article](http://techcrunch.com/2016/02/01/1270130/)


## Audit Committee Charter

1.	Purpose. The purpose of the Audit Committee of the Board of Directors (the “Board”) of GitLab Inc. (the “Company”) is to assist the Board’s oversight of:
    - The integrity of the Company’s financial statements;
    - The performance, qualifications and independence of the Company’s registered public accounting firm (the “external auditors”);
    - The performance of the Company’s internal financial, accounting and reporting controls and other processes.
    - The company's process for monitoring compliance with laws and regulations and the code of conduct.

1.	Structure and Membership
    - Members. The Audit Committee shall consist of at least two members of the Board, each of whom shall be independent.
    - Financial Literacy. Each member of the Audit Committee must be financially literate, as such qualification is interpreted by the Board in its business judgment, or must become financially literate within a reasonable period of time after his or her appointment to the Audit Committee.
    - Chair. Unless the Board elects a Chair of the Audit Committee, the Audit Committee shall elect a Chair by majority vote.
    - Selection and Removal. Members of the Audit Committee shall be appointed by the Board.
1.	Authority and Responsibilities
    - General. The Audit Committee shall discharge its responsibilities, and shall assess the information provided by the Company’s management and the external auditors, in accordance with its business judgment. Management is responsible for the preparation, presentation, and integrity of the Company’s financial statements and for the appropriateness of the accounting principles and reporting policies that are used by the Company. The external auditors are responsible for auditing the Company’s financial statements. The authority and responsibilities set forth in this Charter do not reflect or create any duty or obligation of the Audit Committee to plan or conduct any audit, to determine or certify that the Company’s financial statements are complete, accurate, fairly presented, or in accordance with generally accepted accounting principles or applicable law, or to guarantee the external auditors’ reports.
    - Oversight of Integrity of Financial Statements
    - Review and Discussion. The Audit Committee shall meet to review and discuss with the Company’s management and external auditors the Company’s audited financial statements.
    - Related-Person Transactions. The Audit Committee shall review related-person transactions under the Company’s Related Person Transaction Policy and applicable accounting standards on an ongoing basis and such transactions shall be approved by the Audit Committee.
    - Oversight of Performance, Qualification and Independence of External Auditors
    - Consider the effectiveness of the company's internal control system, including information technology security and control.
    - Understand the scope of internal and external auditors' review of internal control over financial reporting, and obtain reports on significant findings and recommendations, together with management's responses.
    - Review fraud risk assessment of the entity
    - Approve the internal audit charter
    - Approve the annual audit plan and all major changes to the plan. Review the internal audit activity’s performance relative to its plan.
    - Review the effectiveness of the system for monitoring compliance with laws and regulations and the results of management's investigation and follow-up (including disciplinary action) of any instances of noncompliance.
    - Review the findings of any examinations by regulatory agencies, and any auditor observations.
    - Review the process for communicating the code of conduct to company personnel, and for monitoring compliance therewith.
    - Obtain regular updates from management and company legal counsel regarding compliance matters.
    - Review and assess the adequacy of the audit committee charter annually, requesting board approval for proposed changes, and ensure appropriate disclosure as may be required by law or regulation.
    - Review new accounting standards- impact and implementation plan, code of conduct violations including hotline complaints , cybersecurity risk assessment conducted by management , Key contracts and IT initiatives taken by management



1.	Selection. The Audit Committee shall be responsible for appointing, evaluating and, when necessary, terminating the engagement of the external auditors. The Audit Committee may, in its discretion, seek stockholder ratification of the external auditors it appoints.
1.	Independence. The Audit Committee shall assist the Board in its assessment of the independence of the external auditors. In connection with this assessment, the Audit Committee shall, at least annually, obtain and review a report from the external auditors describing relationships between the external auditors and the Company, including the disclosures required by the applicable requirements of the Public Company Accounting Oversight Board regarding the external auditors’ independence. The Audit Committee shall actively engage in dialogue with the external auditors concerning any disclosed relationships or services that might impact the objectivity and independence of the external auditors.
1.	Compensation. The Audit Committee shall be directly responsible for setting the compensation of the external auditors. The Audit Committee is empowered, without further action by the Board, to cause the Company to pay the compensation of the external auditors established by the Audit Committee.
1.	Oversight. The external auditors shall report directly to the Audit Committee and the Audit Committee shall be directly responsible for overseeing the work of the external auditors, including resolution of disagreements between Company management and the external auditors regarding financial reporting.
1.	Procedures and Administration
    - Meetings. The Audit Committee shall meet in person or telephonically as often as it deems necessary in order to perform its responsibilities. The Audit Committee may also act by unanimous written consent in lieu of a meeting. The Audit Committee shall periodically meet separately with: (i) the external auditors and (ii) Company management. The Audit Committee shall keep minutes of its meetings and provide those to the Board of Directors.
1. 	Independent Advisors. The Audit Committee shall have the authority, without further action by the Board, to engage and determine funding for such independent legal, accounting and other advisors as it deems necessary or appropriate to carry out its responsibilities. Such independent advisors may be the regular advisors to the Company. The Audit Committee is empowered, without further action by the Board, to cause the Company to pay the compensation of such advisors as established by the Audit Committee.
1.	Investigations. The Audit Committee shall have the authority to conduct or authorize investigations into any matter within the scope of its responsibilities, as it shall deem appropriate, including the authority to request any officer, employee or advisor of the Company to meet with the Audit Committee or any advisors engaged by the Audit Committee.
1.	Additional Powers. The Audit Committee shall have such other duties as may be delegated.

## Compensation Committee Charter
1.	Purpose
The purpose of the Compensation Committee of the Board of Directors (the “Board”) of GitLab Inc. (the “Company”) is to assist the Board in the performance of its responsibilities relating to the Company’s compensation programs in general and specifically, but not limited to, its’ executive officers.
1.	Structure and Membership
    - Number. The Compensation Committee shall consist of at least two members of the Board.
    - Independence. At least two members of the Compensation Committee shall not have management responsibilities.
    - Chair. Unless the Board elects a Chair of the Compensation Committee, the Compensation Committee shall elect a Chair by majority vote.
    - Compensation. The compensation of Compensation Committee members shall be as determined by the Board.
    - Selection and Removal. Members of the Compensation Committee shall be appointed by the Board. The Board may remove members of the Compensation Committee from such committee, with or without cause, at any time that it determines to do so.
1.	Authority and Responsibilities
    - General. The Compensation Committee shall perform its responsibilities, and shall assess the information provided by the Company's management, in accordance with its business judgment.
    - Compensation Matters
    - CEO Compensation and Performance. The Compensation Committee shall annually review and approve corporate goals and objectives relevant to the compensation of the Company’s Chief Executive Officer (the “CEO”), evaluate the CEO’s performance in light of those goals and objectives, and, either as a committee or together with the other independent directors (as directed from time to time by the Board), determine and approve the CEO’s compensation based on this evaluation.
    - Executive Officer Compensation. The Compensation Committee shall review and approve, or recommend for approval by the Board, executive officer (including the CEO) compensation, including salary, bonus and incentive compensation levels; deferred compensation; executive perquisites; equity compensation (including awards to induce employment); severance arrangements;
change-in-control benefits and other forms of executive officer compensation. The Compensation Committee shall meet without the presence of executive officers when approving or deliberating on CEO compensation but may, in its discretion, invite the CEO to be present during approval of, or deliberations with respect to, other executive officer compensation.
1.  Plan Recommendations and Approvals. The Compensation Committee shall periodically review and make recommendations to the Board with respect to incentive-compensation plans and equity-based plans that are subject to approval by the Board.
1.  Director Compensation. The Compensation Committee shall periodically review and make recommendations to the Board of Directors with respect to director compensation.
1.  Additional Powers. The Compensation Committee shall take such other action with respect to compensation matters as may be delegated from time to time by the Board.
1.  Procedures and Administration
    - Meetings. The Compensation Committee shall meet in person or telephonically as often as it deems necessary in order to perform its responsibilities. The Compensation Committee may also act by unanimous written consent in lieu of a meeting. The Compensation Committee shall keep such records of its meetings and furnish the minutes of such meetings to the Board of Directors.
    - Charter. The Compensation Committee shall periodically review and reassess the adequacy of this Charter and recommend any proposed changes to the Board for approval.
    - Compensation Consultants, Legal Counsel and Other Advisors. The Compensation Committee may, in its sole discretion, retain, terminate or obtain the advice of compensation consultants, legal counsel or other advisors. The Compensation Committee shall be directly responsible for the appointment, compensation and oversight of the work of any compensation consultant, legal counsel and other advisor retained by the Compensation Committee. The Compensation Committee is empowered, without further action by the Board, to cause the Company to pay the compensation, as determined by the Compensation Committee, of any
compensation consultant, legal counsel and other advisor retained by the Compensation Committee. The Compensation Committee may select, or receive advice from, a compensation consultant, legal counsel or other advisor, only after taking into consideration, as applicable, all factors relevant to that person’s independence from management.
1.	Investigations. The Compensation Committee shall have the authority to conduct or authorize investigations into any matters within the scope of its responsibilities as it shall deem appropriate, including the authority to request any officer, employee or advisor of the Company to meet with the Compensation Committee or any advisors engaged by the Compensation Committee.

## Nominating and Corporate Governance Committee Charter
1. Purpose
The purpose of the Nominating and Corporate Governance Committee (the “Committee”) of the Board of Directors (the “Board”) of GITLAB, INC. (the “Company”) is to ensure that the Board is properly constituted to meet its fiduciary obligations to stockholders and the Company, and to assist the Board with respect to corporate governance matters, including:
    - identifying, considering and nominating candidates for membership on the Board; and
    - advising the Board on corporate governance matters and Board performance matters, including recommendations regarding the structure and composition of the Board and Board committees.
This charter (the “Charter”) sets forth the authority and responsibilities of the Committee in fulfilling its purpose.
1. Structure and Membership
The Committee will consist of two or more members of the Board, with the exact number determined from time to time by the Board.  Each member of the Committee will:
    - be free from any relationship that, in the opinion of the Board, would interfere with the exercise of independent judgment as a Committee member; and
    - meet any other requirements imposed by applicable law, regulations or rules, subject to any applicable exemptions.
All members of the Committee will be appointed by, and will serve at the discretion of, the Board.  The Board may appoint a member of the Committee to serve as the chairperson of the Committee (the “Chair”).  If the Board does not appoint a Chair, the Committee members may designate a Chair by their majority vote.  The Chair will work with management to set the agenda for Committee meetings and conduct the proceedings of those meetings.
1. Authority and Responsibilities
The principal responsibilities and duties of the Committee in serving the purposes outlined in Section I of this Charter are set forth below. These duties are set forth as a guide, with the understanding that the Committee will carry them out in a manner that is appropriate given the Company’s needs and circumstances.  The Committee may supplement them as appropriate and may establish policies and procedures from time to time that it deems necessary or advisable in fulfilling its responsibilities.
The responsibilities and authority of the Committee will include:
   - *Nominating Duties*:
     1.	Develop the director nomination processes.  Determine or recommend to the Board for determination the desired qualifications, expertise and characteristics of Board members, with the goal of developing a diverse, experienced and highly qualified Board.  On an ongoing basis, the Committee will consider Board composition factors, including independence, integrity, diversity, age, skills, financial and other expertise, breadth of experience, knowledge about the Company’s business or industry and willingness and ability to devote adequate time and effort to Board responsibilities in the context of the existing composition, other areas that are expected to contribute to the Board’s overall effectiveness and needs of the Board and its committees.
     2. Identify and recruit qualified candidates for Board membership, consistent with criteria approved by the Board.
     3. Oversee inquiries into the backgrounds and qualifications of potential candidates for membership on the Board, including review of the independence of the non-employee directors and members of the Committee, the Audit Committee, the Compensation Committee and other independent committees of the Board.
     4.	Propose recommendations as to the size of the Board.
   - *Corporate Governance Duties*:
     1. Periodically review the business interests and business activities of members of the Board and management.
     2. Recommend that the Board establish special committees as may be desirable or necessary from time to time in order to address interested director, ethical, legal or other matters that may arise.
     3. Consider the Board’s leadership structure, including the separation of the Chairman and Chief Executive Officer roles and/or appointment of a lead independent director of the Board, either permanently or for specific purposes, and make such recommendations to the Board with respect thereto as the Committee deems appropriate.
     4. Make such recommendations to the Board and its committees as the Committee may consider necessary or appropriate and consistent with its purpose, and take such other actions and perform such other services as may be referred to it from time to time by the Board.
     5. From time to time, review this Charter and the Committee’s performance, and in the event that the Company intends to begin preparation for an initial public offering or as a result of such review, make recommendations to the Board regarding revisions to this Charter as appropriate.
1. Studies and Advisors
The Committee, in discharging its responsibilities, may conduct, direct, supervise or authorize studies of, or investigations into, matters within the Committee’s scope of responsibility, with full and unrestricted access to all books, records, documents, facilities and personnel of the Company.  The Committee has the sole authority and right, at the expense of the Company, to retain legal counsel and other consultants, accountants, experts and advisors of its choice to assist the Committee in connection with its functions, including any studies or investigations.  The Committee will have the sole authority to approve the fees and other retention terms of such advisors.  The Company will provide for appropriate funding, as determined by the Committee, for:
    -	payment of compensation to any search firm, legal counsel and other consultants, accountants, experts and advisors retained by the Committee; and
    -	ordinary administrative expenses of the Committee that are necessary and appropriate in carrying out its functions.
Irrespective of the retention of legal and other consultants, accountants, experts and other advisors to assist the Committee, the Committee shall exercise its own judgment in fulfillment of its functions.
1. Meetings, Actions Without A Meeting And Staff
The Committee will meet with such frequency as is determined appropriate by the Committee.  The Chair, in consultation with the other member(s) of the Committee, will set the dates, times and places of such meetings.  The Chair or any other member of the Committee may call meetings of the Committee by notice in accordance with the Company’s Bylaws.  A quorum of the Committee for the transaction of business will be a majority of its members.  Meetings may be held via tele- or video-conference.  The Committee may also act by unanimous written consent in lieu of a meeting in accordance with the Company’s Bylaws.  Subject to the requirements of this Charter, and applicable law, rules and regulations, the Committee and the Chair may invite any director, executive or employee of the Company, or such other person, as it deems appropriate in order to carry out its responsibilities, to attend and participate (in a non-voting capacity) in all or a portion of any Committee meeting.  The Committee may exclude from all or a portion of its meetings any person it deems appropriate in order to carry out its responsibilities.  The Chair will designate a secretary for each meeting, who need not be a member of the Committee.  The Company will provide the Committee such staff support as it may require.
1. Minutes and Reports
The Committee will maintain written minutes of its meetings and copies of its actions by written consent, and will make such minutes and copies of written consents available to the other members of the Board and cause them to be filed with the minutes of the meetings of the Board.  The Chair will report to the Board from time to time with respect to the activities of the Committee, including on significant matters related to the Committee’s responsibilities and the Committee’s deliberations and actions.
1. Delegation of Authority
The Committee may from time to time, as it deems appropriate and to the extent permitted under applicable law, and the Company’s Certificate of Incorporation and Bylaws, form and delegate authority to subcommittees.
1. Compensation
Members of the Committee will receive such fees, if any, for their service as Committee members as may be determined by the Board, which may include additional compensation for the Chair.
