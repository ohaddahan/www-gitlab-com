---
layout: handbook-page-toc
title: Compensation
description: Find answers to your questions about GitLab's compensation framework.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Related Compensation Pages

* [Compensation Calculator Formula](/handbook/total-rewards/compensation/compensation-calculator)
* [Use the Compensation Calculator](/handbook/total-rewards/compensation/compensation-calculator/calculator)
* [Compensation Review Cycle](/handbook/total-rewards/compensation/compensation-review-cycle)

## Introduction
On this page, we're detailing why we have the compensation framework we have now.

## GitLab's Compensation Principles

1. We're an open organization, and we want to be as transparent as possible about our compensation principles. Our compensation model is open to data driven iterations.
1. We are [paying local rates](#paying-local-rates).
1. Compensation aims to be at a [competitive rate](#competitive-rate) for your job family, your location, your level, your experience, and your contract type.
1. We use a [compensation calculator](/handbook/total-rewards/compensation/compensation-calculator) to ensure transparent and consistent compensation.
1. We do not disclose individual compensation since compensation is [not public](/handbook/communication/#not-public).
1. We adjust our calculator based on survey data, feedback from applicants and team members, and candidate data. Please email total-rewards@ domain if you want to contribute.
1. We offer [stock options](/handbook/stock-options/) for most positions.
1. We base compensation on current position, experience at GitLab, and performance -- not on what we paid you last month -- and, generally, we don't reduce compensation.
1. We want to follow the processes on this page for everyone, please email total-rewards@domain when we don't. If you have a specific question around your compensation or one of your direct reports' please schedule a call with total-rewards@ domain to review.
1. We will update this page and the processes throughout the year.
1. We'll adjust your pay as soon as your job-family or level factor changes.
1. If we change our [SF benchmark](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark) for a job family without changing the requirements, we change the compensation both for existing team members and new hires. If the SF benchmark is changed together with the requirements this review might happen at the time of the change or in our yearly cycle.
1. We offer [bonuses and incentives](/handbook/incentives), but we don't offer ping pong tables or free lunches. We think it is more important to offer people flexibility and freedom. See the [Top 10 reasons to work for GitLab on our culture page](/company/culture/#advantages/).
1. We hire across the globe, but we're not location agnostic. Your timezone, the location factor in your region, and the vicinity to users, customers, and partners can all be factors. For example, we may favor one applicant over another because they live in a region with a lower location factor or because we need someone in that timezone. All things being equal, we will hire people in lower cost markets vs. higher cost markets.
1. People on quota (account executives, account managers, and sales leadership) have variable compensation that is about 50% of their On Target Earnings (OTE). Individual contributors in the sales organization have variable compensation that is purely based on commission. The commission for all roles in the sales function is paid monthly with the exception of the following roles: Professional Services Engineer, Sales Development Manager, Director of Customer Success. These roles are paid commission quarterly. The VP of Alliances is paid according to the Executive Bonus Program.
1. Compensation decisions around level and experience levels and for functions not in the calculator are taken by the compensation group<a name="compensation-group"></a>. This group consists of the CFO, CEO, and Chief People Officer. All requests will first be routed to the CFO and the CPO. If needed, the request can be escalated to the CEO. When there is no time to coordinate with the group, the CEO can make a decision and inform the group. When the CEO is away (e.g. vacation), the two other members of the group can make a decision and inform the group. Whatever the decision is, the earnings committee should be cc-ed (or bcc-ed) on the final email so that the committee members can know that the loop was closed. This group is different from the compensation committee at the [board level](/handbook/board-meetings/#board-and-committee-composition).

## Competitive Rate

**We want our compensation to be at a level where we can recruit and retain people who meet our requirements.**
Our requirements for most of our [job-families](/handbook/hiring/job-families/) are at **or above** the average in the market.
Therefore, we can expect to be at or above the 50th percentile of the survey data gathered from providers like Comptryx and Radford.
Please do not use the term market rate since this can mean either competitive rate or survey data.
Also see our [SF benchmark](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark).

When discussing a competitive rate for a single person at GitLab, please refer to this as their lottery factor. For example, if this person won the lottery and left the company what impact would that have. Other common industry terms are walk away factor or bus factor, but those both hold a negative connotation.

## Paying Local Rates

### Why we pay local rates

Competitive rates for roles vary depending on regions and countries.
We pay a [competitive rate](#competitive-rate) instead of paying the same wage for the same role in different regions.
Paying the same wage in different regions would lead to:

1. A concentration of team members in low-wage regions, since it is a better deal for them, while we want a geographically diverse team.
1. Team members in high-wage regions having less discretionary income than ones in low-wage countries with the same role.
1. Team members in low-wage regions being in [golden handcuffs](https://en.wikipedia.org/wiki/Golden_handcuffs) and sticking around because of the compensation even when they are unhappy. We believe that it is healthy for the company when unhappy people leave.
1. If we start paying everyone the highest wage our compensation costs would increase greatly, we can hire fewer people, and we would get less results.
1. If we start paying everyone the lowest wage we would not be able to attract and retain people in high-wage regions.

For more context please also see our [blog post about paying local rates](https://about.gitlab.com/blog/2019/02/28/why-we-pay-local-rates/).

Please see a Twitter thread from compensation expert Heather Doshay where she discusses [paying local rates](https://twitter.com/heatherdoshay/status/1228743322350473217?s=12) and how that impacts both companies and team members.

### Hire the best candidate

We hire the best candidate for each role regardless of location, cost, or other factors.
During the sourcing we do optimize what potential candidates we approach in order to bring more diversity (both geographically and people from underrepresented backgrounds) to our team.

### Relocating

Different than business travel, vacation, or visiting a relative for a few weeks, relocation means that you will establish yourself in a new location outside of your current metro area. If you are ending your current residential living arrangement, spending more than six months in one location as part of an extensive period of travel and/or will have your mail delivered to an address in a different city please contact us.

As stated in the [code of conduct section of the handbook](/handbook/people-group/code-of-conduct/#relocation), you should first obtain written agreement (from your manager) when planning a relocation. It is the company's discretion to offer you a contract in your new location. At the time of the location update, we will take into consideration your new metro region when making a salary offer for continued employment.

At the onset, this practice sounds harsh when moving to a lower paid region. One might argue that it seems unfair for the organization to pay someone less for the same work in the same role, regardless of where they go. However, if you look at it from another angle for a minute and compare this practice to what most companies do, it should make more sense. For example, say you work for a company with physical locations and say they haven't accepted that remote work is as productive as coming into the office yet. If you wanted to pack up and move to a location where they did not have a physical site, you would have no alternative but to resign and seek new employment in your new location. You would find quickly that companies in the area pay at a locally competitive rate.

Now, let's say the company did have a site in your new location and they offered the flexibility to transfer. If they did not have a similar position open, you would have to either apply for a different open position in the same company or resign and apply externally (back to the realization that other companies will pay at local competitive rate). If you were lucky enough that they did have a similar role in the new location, a transfer would come with a pay rate based on the local market to ensure equity across all incumbents (people in the job) by location.

Adjusting pay according to the local market in all cases is fair to everyone.  We can't remain consistent if we make exceptions to the policy and allow someone to make greater than local competitive rate for the same work others in that region are doing (or will be hired to do).  We realize we might lose a few good people over this pay policy, but being fair to **all** team members is not negotiable.  It is a value we stand behind and take very seriously.

Please refer to the [code of conduct](/handbook/people-group/code-of-conduct/#relocation) to review the relocation process.

**For Total Rewards: Processing Information Update Requests:**

1. If the request is a new address entry for new hires or a new hire editing the spelling or format of their address, approve the request.
1. If the team member is in sales:
  * Update the appropriate payroll file (no need to follow this step for contractors)
  * Update the FY21 Comp Data Analysis and Modeling sheet
  * For US team members - update the locality in BambooHR
  * For US team members - update the benefit group if required
1. If the team member is not in sales:
  * An [approval email](/handbook/people-group/code-of-conduct/#relocation) is required. Lower or equivalent location factor relocations require approvals from the manager and the assigned People Business Partner. Higher location factor relocations require approvals from the People Business Partner and the group Executives approval. 
  * If a compensation change is required, update the team member's compensation details in BambooHR and stage a letter of adjustment in HelloSign
  * Update the appropriate payroll file (no need to follow this step for contractors)
  * Update the FY21 Comp Data Analysis and Modeling sheet
  * For US team members - update the locality in BambooHR
  * For US team members - update the benefit group (if required)

### Blog Post

We also wrote a [blog post about paying local rates](/blog/2019/02/28/why-we-pay-local-rates/).

## Market-Based Approach

We have a market-based approach to compensation because:
1. It allows for us to adopt to market changes, thus remaining competitive for local talent
1. Role, location, level, and compa group benchmarks keep compensation bands consistent. Performance-based pay leaves more room for bias as managers may not be consistent at reviewing their team members, allowing for inequity where one team member with the same attributes gets more compensation than the other. Market-based benchmarks also help with keeping it fair and consistent rather than expecting our team members to negotiate their pay.
1. Prevents wage compression which is when new employees negotiate and get higher wages than those being paid to current team members.
1. As long as budget permits, we do what’s right for the market even if it means paying a team member higher than their “expected” pay.

## Compensation for Interim Roles

Effective Q2 of FY 2021, we have established a one time bonus payment process for team members that are asked to step into an interim management role. Team Members acting in an interim management role should review [the expectations of an individual in the managament group](/handbook/leadership/#management-group). For an interim role to be considered, the need for coverage would need to be longer than a 30 day time period.

The formula for the bonus recognizes the length of time that the team member is playing the interim role. Payment of the one time bonus would occur at the completion of the interim role. The bonus would be calculated using the following formula:

The greater value of the standard discretionary bonus amount ($1,000 at the current exchange rate) OR the following calculation:

For team members on a base salary compensation plan, your bonus will be an additional 10% of your salary for the duration of the interim role period.  The calculation is as follows:
* `(Annual Base Salary in Local Currency/365) x .10 (10%) x # of Calendar Days in the Interim Role`

For team members on an OTE (On Target Earnings) compensation plan, if you assume the quota of the role that you are covering for, you will be paid 10% of your OTE for the interim role period. The calculation is as follows:
* `(Annual On Target Earnings in Local Currency/365) x .10 (10%) x # of Calendar Days in the Interim Role`
* Commissions payments will continue for the duration of the interim role. Once the interim role is complete, your quota will revert to the quota of your previous role and a new compensation plan will be issued memorializing that quota and commission rate.

The process for submitting an interim bonus is as follows:
* Leader works with their aligned People Business Partner to confirm the start and end dates of the interim role.
* The People Business Partner works with the Compensation group to confirm the bonus amount.
* Once confirmed, the leader would [submit the bonus in BambooHR](/handbook/incentives/#process-for-recommending-a-team-member-for-a-bonus-in-bamboohr).

The team member must be an active team member of Gitlab at the end of the interim role period to be eligible to recive a bonus payment. If a team member leaves GitLab during the interim role period, they will not be eligible for a prorated payment.

### Examples of the Interim Bonus Calculations below

* Senior Engineer has a base salary of $125,000. She has taken on the interim role of Engineering Mgr for 3 months (Jan-March) which is a total of 90 days. The bonus for this interim role would be `($125,000/365) x .10 x 90 = $3,082.19`
* Finance Business Partner has a base salary of $100,000. He has taken on an interim role covering multiple teams while his coworker is on leave for 4.5 weeks which is a total of 31 days. The bonus for this interim role would be `($100,000/365) x .10 x 31 = $849.32` so we would round up for this bonus and process as a discretionary award.


## Exchange Rates

<%= partial "includes/people-operations/currency_exchange_listing" %>

### Paid in your local currency

The compensation calculator is updated in January and July with the proper exchange rate, keeping compensation levels in line with local purchasing power.

### Not paid in your local currency

There are a number of reasons why team members may not be paid in local currency. For example, GitLab's bank might not support the currency or there may be a strong economic reason for the team member to choose a different currency. The currencies that GitLab can pay in are published on the [contracts](/handbook/contracts#available-currencies) page.

1. The Total Rewards Analyst will analyze the difference in exchange rates from January to July (or vice versa) and the effect this has on local purchasing power using the following process:
  * The exchange rate between the team member's local currency and chosen currency will be collected for both the current and previous conversion effective date.
  * The percent change is calculated between the two rates: `(New Rate - Previous Rate)/Previous Rate`.
1. Criteria for review, the team member:
  * Is not paid in local currency.
  * Has been at GitLab for greater than six months.
  * Has not opted out.
1. Only changes greater than +/- 5% will be processed in each review.
  * Changes must stay within band of the compensation calculator.
1. Total Rewards Analysts will confirm by [letter of adjustment](/handbook/contracts/#letter-of-adjustment) any _increases or decreases_ to compensation as a result of this review.
1. Any significant increases or decreases will be reviewed on a case-by-case basis before any changes are made.
1. Team members may also [opt out](https://docs.google.com/document/d/1GMxFFMKL8ssEi8gPJzpc1xizSJHxrR2n0R5HSZn-G-g/edit?usp=sharing) of the reviews.
  * If an opt out agreement is signed, the team members will be excluded from any future Exchange Rate reviews. GitLab retains the right to revisit the opt-out status in the future.
1. Additionally, if a team member is impacted outside of this review period they should reach out to total-rewards@domain.

Example:  A team member's local currency is the Russian Ruble (RUB) and they are paid in US Dollars (USD). They have not previously opted out and have been employed for greater than 6 months. The exchange rate effective 2020-01-01 is 0.016. If for 2020-07-01 the exchange rate increases to 0.017, then this would result in a percent increase of 6.25%. The team member would have the option to either accept this increase to their salary or opt out.

## Director Compensation

In addition to base compensation, Directors who are not already enrolled in the Sales Compensation Plan or other performance incentive plan are eligible for a 15% bonus of current base salary (increased from 10% beginning in FY 2021). Director Compensation is determined as part of the [GitLab Compensation Calculator](/handbook/total-rewards/compensation/compensation-calculator/calculator/).

## Executive Compensation

Executive Compensation (VP and above) is derived outside of the GitLab Compensation Calculator using the following process:

1. The Total Rewards team collects market data to review against current compensation to ensure alignement in base and variable.
  * Market Data from the GitLab peer group: In FY21 the peer group data used came from Radford, in FY22 the peer group data will be from Compensia.
  * AdvancedHR Market Data for a Private Pre-IPO Company with 5+ rounds of funding.
1. The Total Rewards team will advise on a market increase or prorated Cost of Living Adjustment, whichever is appropriate.
1. The recommendation will be reviewed by the Compensation Group ensuring alignment to budget with the CFO.
1. If approved by the Compensation Group, the Total Rewards team will escalate the CEO's direct reports to the Compensation Committee for approval.
1. The Total Rewards team will then notify the CEO or direct manager of the increase who will then communicate it to the individual.

For FY22, GitLab will review how we can incorporate performance into the Executive Compensation increase process.

As each Executive has an individual market assessment upon hire, we expect compensation to be aligned to market at each compensation review. If there are large changes in the market for a specific role, those will be addressed.

## Director and above Bonus Plan

Targets:
50% of the bonus is based on performance against [IACV](/handbook/sales/#incremental-annual-contract-value-iacv).
50% of the bonus based on performannce against [TCV](handbook/sales/#total-contract-value-tcv) less Operating Expenses (including COGS).

All targets are based on Company performance against Plan targets as approved by the Board of Directors.

**Thresholds:**
* Each target is subject to a threshold. That is, if Company performance is less than the threshold no bonus is earned. Above the threshold the bonus is earned based on the percentage achievement above or below the threshold.  For example at 90% of Plan the bonus is achieved at 90%; at 110% of Plan the bonus is achieved at 110%.
* The IACV target carries an 80% threshold and the TCV less OpEx carries a 90% threshold.
* The thresholds operate independently such that if one component is not achieved bonus may still be earned on the other component.
* Each target is subject to a cap of 200%.

**Payout:**
* The bonus will be paid out semi-annually.
* The first semi-annual payment will be at the lower of Company performance against targets or 100% of plan performance.
* The final bonus payout will be based on actual performance against plan less amount paid at the semi-annual payout.
* If the first semi-annual bonus is achieved but Company performance falls below the annual threshold the second half bonus will not be achieved but the first half bonus will not be rescinded or otherwise payable back to the Company.

**Timing:**
* The first payment is expected to occur within 30 days after the end of the second quater of the fiscal year.
* The second payment is expected to occur within 60 days after the end of the fiscal year.

**Approvals:**
* The bonus plan is approved by the [Compensation Committee](/handbook/board-meetings/#compensation-committee) of the Board of Directors.
* Performance against Plan targets is reviewed and approved by the [Audit Committee](/handbook/board-meetings/#audit-committee) of the Board of Directors.
* Bonus payouts are recommended by the CFO and approved by the Compenesation Committee of the Board of Directors.

**Proration and Eligibility:**
* The Director bonus will be pro rated for those team members who start after Feb 1st, or are promoted after August 1st.
* In addition, the team member must be actively employed by GitLab at the time of payout.

Bonus examples can be found in the following [google doc](https://docs.google.com/document/d/1G_CQQ8GBtqFvwUlOH8WpxddqPMCH2HdHbl1I_fJ-jB0/edit?usp=sharing).

GitLab reserves the right to change or discontinue the bonus program at any time. All bonus payouts are discretionary and require the achieving of specific company results that will be communicated each year.

### Bonus Payout Approvals

The Compensation Committee approves all bonus payouts for direct reports to the CEO at the end of each Fiscal Year. The Compensation Group internally at GitLab approves all other bonus payouts.

1. The Finance team will generate a calculation of the total bonus payout for the Fiscal Year within 30 days of the last day of the fiscal year.
1. The Total Rewards Team will audit the calculation.
1. Once approved by the Total Rewards Team, the CFO and CPO will review and approve all final numbers.
1. Once approved by the CPO and CFO, the Total Rewards Team will generate a google sheet summarizing the data for the direct reports of the CEO. This sheet should include the following headers: Employee #, Name, Reporting to, Division, Department, Title, Hire Date, Total Eligible, Total Payout. Any additional notes should be added to the bottom of the sheet.
1. The Total Rewards Team will communicate the final numbers via email to the Compensation Committee for approvals and ccing the CFO, CPO, CEO, and CLO in the communication.
1. Once approved by the Compensation Committee, the Total Rewards team will notify payroll that the bonuses are ready for processing.
1. Once the compensation group internally at GitLab approves all indirect reports to the CEO, the Total Rewards Team will notify payroll that the bonuses are ready for processing.

## Learning GitLab's Compensation Framework

As part of our Q1 OKR, we will be working on ensuring there are materials for a compensation certification. The following are the initial questions to generate the certification.

### The Why Questions

* [Why do we have the compensation framework we have now?](/handbook/total-rewards/compensation/#gitlabs-compensation-principles)
* [Why does GitLab aim to pay a competitive rate?](/handbook/total-rewards/compensation/#competitive-rate)
* [Why does GitLab pay local rates?](/handbook/total-rewards/compensation/#paying-local-rates)
* [Why does GitLab have a market-based approach to compensation instead of performance-based?](/handbook/total-rewards/compensation/#market-based-approach)
* [Why does GitLab have a Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#the-compensation-calculator)

### The What Questions

* [What is the foundation of GitLab's compensation framework?](/handbook/total-rewards/compensation/compensation-calculator/#the-compensation-calculator)
* [What is the formula of the Compensation Calculator or otherwise known as the Calculator "Inputs" or "Factors"?](/handbook/total-rewards/compensation/compensation-calculator/#the-compensation-calculator-formula)
* [What does the SF Benchmark mean on the Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark)
* [What does the Location Factor mean on the Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#location-factor)
* [What does the Compa Ratio mean on the Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#compa-ratio)
* [What does the Contract Factor mean on the Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#contract-factor)
* [What does the Exchange Rate mean on the Compensation Calculator?](/handbook/total-rewards/compensation/#exchange-rates)

### The How Questions

* [How do we continue to make sure that our team members are compensated according to their skill level and receive equitable pay?](/handbook/total-rewards/compensation/compensation-review-cycle/#compensation-review-cycle)
* [How do we make sure that the different Compensation Calculator inputs remain relevant and competitive to market?](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review)
* [How does the Total Rewards team carry out the Annual Compensation Review?](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review-timeline)
* [How do we make sure that new team members joining GitLab between November and January will receive a compensation review every 12 months?](handbook/total-rewards/compensation/compensation-review-cycle/#catch-up-compensation-review)

## Knowledge Checks

You can test your knowledge on our compensation by taking the [GitLab Compensation Knowledge Assessment](https://docs.google.com/forms/d/e/1FAIpQLSe7cf8j9EMk3raWY7nHwSSVSdO0eE7bSHZ8TxSbm-L7hgQvuw/viewform) quiz.

If you have qustions about compensation or the content in the Knowledge Assessment, please reach out to the [Total Rewards](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group) team. If the quiz is not working or you have not recieved your certificate after passing the Knowledge Assessment, please reach out to the Learning & Development team.
