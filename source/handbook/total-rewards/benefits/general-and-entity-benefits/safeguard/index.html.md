---
layout: handbook-page-toc
title: "Safeguard"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-group/).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The following benefits are provided by [Safeguard](https://www.safeguardglobal.com/) and apply to team members who are contracted through Safeguard. If there are any questions, these should be directed to People Operations at GitLab who will then contact the appropriate individual at Safeguard.

## Ireland

- Currently Safeguard does not provide private healthcare
  * If GitLab did want to look into implementing private healthcare with Safeguard, the costs would be plan dependent and based on level of cover and workers age.
    - Providers ask for application forms to be completed by workers in the first instance in order to obtain individual quotes. Providers will not quote without completed application forms.
  * GitLab will continue to review implementing Medical cover in Ireland.
- Safeguard does provide a pension via Zurich, if individuals would like to join this scheme, a leaflet can be found by clicking on this [link](https://drive.google.com/file/d/1GRasMwjchtKSw4ZkPJNJsiCjAU-MOsNH/view?usp=sharing). Please note that at this time there are no employer contributions.
  * Any changes to the scheme would need to be documented via Safeguard through a contract variation.
- Safeguard does not offer Tax Saver & Bike to Work benefits.
  * As these schemes are salary sacrifice Safeguard would need to know in advance of any set up what the level of take-up would be to see if set up was viable.
  * GitLab will continue to review offering the Tax Saver & Bike to Work benefits.
- Safeguard does not offer Life Assurance.
  * Safeguard would need to generate a plan specific to GitLab. GitLab and Safeguard will continue to review securing a separate policy for Gitlab.
- Paternity Leave in Ireland:
  *  The Paternity Leave and Benefit Act 2016 provides two weeks' statutory leave and benefit for employees who are the relevant parents of children born or adopted.
  *  Paternity leave generally must be taken in one block of two weeks within the first six months following the birth or adoption placement. The term "relevant parent" is defined broadly to provide for fathers, adoptive fathers, civil partners, cohabitants and same-sex couples.
  *  Paternity benefit is payable by the state subject to an employee's PRSI contributions. Workers should apply for the payment 4 weeks before they intend to go on paternity leave.
  *  State paternity benefit is payable at a rate of 245 EUR per week for two weeks.
  *  Leave certification - All employees must have their paternity leave certified by their employer. If you're an employee, you must provide GitLab with proof of the expected date of confinement of your spouse or partner. In other words, you will be required to provide your employer with a certificate from your spouse or partner's doctor confirming when your baby is due, or confirmation of the baby’s actual date of birth if you apply for leave after the birth has occurred. GitLab must then complete a form [(PB2: Employer Certificate for Paternity Benefit (pdf))](http://www.welfare.ie/en/pdf/PB2.pdf) to confirm that you are entitled to paternity leave.
  *  [If you have been at GitLab for a year, GitLab will supplement Paternity Pay up to the full salary amount](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).
  *  Statutory Paternity Pay can be processed in two ways:
     *  By electronic funds transfer into the employee's nominated bank account, or,
     *  Mandated to GitLab's account where we would process through payroll along with any supplementary amount (if you are eligible).
    Please notify the Compensation & Benefits team how you would like to receive the Statutory Payment.

## Spain

_We are currently unable to hire any more employees or contractors in Spain. Please see [Country Hiring Guidelines](/jobs/faq/#country-hiring-guidelines) for more information._

- Currently Safeguard does not provide private healthcare
- Accruals for 13th and 14th month salaries
- General risks and unemployment insurance
- Salary guarantee fund (FOGASA)
- Work accident insurance

 GitLab currently has no plans to offer Life Insurance, Private Medical cover, or a private pension due to the government cover in Spain. If GitLab did want to look into offering such benefits through Safeguard, the employees would need to subscribe themselves to the insurance companies and they would be paid for it in the form of allowance on a monthly basis covering the price that the insurance company requests; this would then be billed to Gitlab.

## Hungary

- Healthcare insurance according to Hungarian laws
- Pension according to Hungarian laws

> You may receive your payslip from this email address: abacus@whc.hu on your personal email ( the one used for correspondence with Safeguard). The payslip is going to contain an encrypted PDF.


## Other countries

Safeguard uses multiple third parties across the globe to assist in locations where they do not have a direct payroll.

## Switzerland

As per social security obligations, for its part, the employer pays:
- half of the total premium of the AVS/AI/APG
- the whole of the accident insurance contribution (professional)
- half of the loss of earnings insurance in the event of sickness
- half the total unemployment benefit insurance premium
- half the total maternity insurance premium
- part of training and execution
- LPP Silver
- During the execution of a mission at a client of the employer, the employee is insured against the risks of occupational accidents at the CNA. If it works 8 hours or more per week, it is also insured against the risks of nonprofessional accidents at the SUVA, according to the legal provisions in force. The SUVA benefits replace the obligation to pay wages according to art. 32a CO. The waiting period is 3 days from the accident.
- Employees are entitled to maternity benefits, in accordance with article 16b of the law on allowances for losses of earnings, LAPG, if they have been compulsorily insured within the meaning of the old-age and survivors insurance, LAVS, during the 9 months preceding the childbirth and that during this period, they were in gainful employment lasting 5 months and are still paid at the date of childbirth. The right to the allowance takes effect from the day of childbirth. The mother benefits from maternity leave up to a maximum of 14 weeks paid at a rate of 80% of the average earnings for the activity carried out before childbirth. The benefit is paid in the form of a daily subsistence allowance (max. 98 daily subsistence allowances). If the mother takes up her gainful employment again during this period, the right is extinguished early.
