---
layout: handbook-page-toc
title: "PTY Australia Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## GitLab PTY Specific Benefits

## Medical
GitLab does not plan to offer Private Health Insurance at this time because Australians can access the public health system for free or at a lower cost through Medicare (funded by tax).

## Pension
GitLab will make superannuation contributions directly to the employee's nominated super fund according to the Australian Government Super Guarantee rate which is currently set at 9.5% of the employee's total salary. Super is on top of the salary listed in the compensation calculator/contract.

### Superannuation Salary Sacrifice
Team members in Australia have the option to make concessional super contributions by sacrificing some of their pre-tax pay and arrange to have it paid into their superfund instead. The combined total of GitLab and salary sacrificed contributions [must not be more than $25,000](https://moneysmart.gov.au/grow-your-super/super-contributions) per financial year. 

*Salary Sacrifice FAQ*

* How do I make concessional contributions to my superannuation?
  * Email total-rewards@gitlab.com with the percentage or amount of your salary that you would like to sacrifice.
* Can I change the amount or opt out?
  * Yes, if you wish to change the amount/percentage or opt out, simply send total-rewards@gitlab.com an email.
* Is it possible to start from any month?
  * Yes, it would be processed on the next available payroll.


## Life insurance
GitLab does not plan to offer life insurance at this time as Australians can access [government payments and services](https://www.humanservices.gov.au/individuals/subjects/payments-people-living-illness-injury-or-disability) if they get ill, injured or have a disability. Most Australians who choose to have life insurance take out cover from their super fund.

## GitLab PTY Australia Leave Policy

### Parental Leave Administrative Details

**Statutory General Entitlement:**
* Employees are able to take parental leave if they:
  * have worked for their employer for at least 12 months before the date or expected date of birth if the employee is pregnant; and
  * have or will have responsibility for the care of a child.

**Australian Government Paid Parental Leave Scheme:**
* Eligible employees who are the primary carer of a newborn or adopted child get up to 18 weeks' leave paid at the [national minimum wage](https://www.humanservices.gov.au/individuals/services/centrelink/parental-leave-pay). If the employee has been at GitLab for one year, the Parental Leave paid through GitLab will be the regular salary minus the payments made from the national minimum wage. The employee will apply online by [claiming](https://www.humanservices.gov.au/individuals/services/centrelink/parental-leave-pay/claiming) the benefit. If you are not eligible for the government benefit, but you are eligible for 100% of parental leave pay, please reach out to People Ops for review.

**Record-keeping for paid parental leave:**
* In addition to the usual record-keeping requirements, employers that have employees getting government-funded Parental Leave Pay also have to keep the following records:
  * the amount of Parental Leave Pay funding received from the government for each employee and the period it covers
  * the date each parental leave payment was made to the employee
  * the period each payment covers
  * the gross amount of the payment
  * the net amount paid and the amount of income tax withheld (including other payments, if any, were made)
  * a statement identifying the payment as Parental Leave Pay under the Australian Government Paid Parental Leave Scheme
  * the amount of any deductions made from each payment.

**Pay slips for parental leave payments:**
* Employees who get Parental Leave Pay have to be given a pay slip for each payment. The pay slip must specify that the payments are Parental Leave Pay under the Australian Government Paid Parental Leave Scheme.
* Ordinary pay slip requirements apply to pay slips given to employees getting government-funded Parental Leave Pay. They must also include:
  * the gross and net amounts of Parental Leave Pay and the amount of income tax deducted
  * if there are other payments on the pay slip, this information must be included as well as the total gross, net and income tax amounts
  * the amount of any deduction and the name and bank details of the entity the deduction was given to.
    * Only certain deductions can be made from Parental Leave Pay under the Australian Government Paid Parental Leave Scheme.

### Applying for parental leave

**Notice requirements:**
* Employees who want to take unpaid parental leave need to give their employer notice that they are taking leave and confirm the dates.
* If an employee can’t give the appropriate notice (eg. the baby is born prematurely) they will still be entitled to take the leave as long as they provide notice when they can.

**10 weeks before starting leave:**
* An employee has to give notice to their employer at least 10 weeks before starting their unpaid parental leave. This notice needs to be in writing, and say how much leave they want to take, including the starting and finishing dates. If an employee can’t give 10 weeks’ notice, they need to provide as much notice as possible.

**4 weeks before starting leave:**
* An employee has to confirm their parental leave dates with their employer at least 4 weeks before they are due to start their leave. If there have been any changes to the dates the employee should tell their employer as soon as possible. If an employee can’t provide 4 weeks’ notice, they need to provide as much notice as possible.
