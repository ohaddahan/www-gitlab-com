---
layout: handbook-page-toc
title: "Customer Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

# Customer Onboarding

Customer Onboarding is the [beginning phase of the customer lifecycle](/handbook/customer-success/vision/#lifecycle-stages).

When a customer purchases or upgrades GitLab to an ARR of $50,000 or greater, a Call To Action (CTA) is triggered within Gainsight for new Enterprise (50k+ ARR) and Mid-Market (20k+ ARR) accounts. A CTA is created for the TAM Manager if the TAM field is not populated. Once populated, an Onboarding CTA is kicked off for the TAM. The Onboarding CTA creates a Playbook with guided steps for the next several weeks. 

The CTA guides the TAM through the tasks for the first part of the journey, examples include:

1. Complete internal transition with the SAL/AE and SA, covering the Command Plan, adoption goals and priorities and stakeholders
1. Hold kickoff call
1. Orient the customer to using GitLab, including best practices
1. ...and more

Use the Gainsight TAM Dashboard to manage customers currently in Onboarding. 

Please also review our [TAM and Support interaction](/handbook/customer-success/tam/support) page as well as our [Support](/handbook/support) handbook section to assist with sharing support information with customers.

While an Onboarding CTA is an automated process, it can also be created manually by going to the Cockpit, clicking `+ CTA` and then choosing the Onboarding playbook. 

## Gainsight C360 Fields

During Onboarding, the TAM should also review the fields in the C360 to create a full picture. Here are a few examples:

1. Fields in the Attributes section
1. TAM Sentiment
1. [Stage Adoption](/handbook/customer-success/tam/stage-adoption/)
1. [Customer Deployment Types](/handbook/customer-success/using-gainsight-within-customer-success/deployment-types/)