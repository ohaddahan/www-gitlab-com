---
layout: handbook-page-toc
title: "Professional Services Instructional Design and Development"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Workflow Labels

Here are the main labels used for PS instructional development projects.

`ProServ-practice::Education`

`Workflow::scheduling` - ID creates build plan

`Workflow::ready for development` - ID determines project is ready for development work to begin

`Workflow::in dev` - The individual components for the Education Services solution are being designed and developed

`Workflow::ready for review`

`Workflow::in review`

`Workflow::blocked`

`Workflow::verification` - This represents the pilot delivery phase for an educational offering

`Workflow::staging`

`Workflow::production` - The Education Services solution is live and can be accessed by the target audience
