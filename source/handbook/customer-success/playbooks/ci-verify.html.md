---
layout: handbook-page-toc
title: "Continuous Integration (CI) / Verify Stage Playbook"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [Customer Success homepage](/handbook/customer-success/) for additional Customer Success handbook content.

---

### Procedure 

The following are the recommended steps to discover, position, lead value discussions, and drive adoption (TAM only).


### Discovery

The following are recommended questions for discovering customer needs.

 ### Talk Track

*“We are looking to better understand across our customer base some of the challenges they may be facing with their current CI solution, and some of the roadblocks they may encounter in adopting Gitlab for their CI, especially considering it is already available to you at no additional cost in the platform that you have. Would it be ok to ask you a few questions on this topic? The insights will help us to determine how to better support our customers."*

**First Round of Questions**

- We have heard that some customers struggle with managing complex pipelines and supporting integrations. What difficulties do you and/or your team see in these areas? 
- Do you have a sense of how much time the team is spending?Many of our customers have multiple Jenkins administrators. Is this the case with you? What if you could free half of those people up to do more than manage pipelines?
- How happy do you believe your teams are with the usability and interface of their current CI solution?
- How often is your day interrupted to fix or maintain your CI tool?
- Tell me about how much "babysitting" your team has to do on your pipeline jobs?
- What kind of roadblocks or hurdles do you encounter when automating builds/tests at scale? What effect is this having on velocity as your scale?
- What benefits do you see in a team being able to self-service a working pipeline within 1 hour and still have the confidence that all standards & best practices are included from day 1?
- (And finally!)Would you be open to scheduling a follow-up call to discuss more around what Gitlab CI can do for you and your team?

**Additional Questions**

- Has there been any discussion to standardize on a single solution for CI since you’re already using GitLab for other needs?
- What is your strategy around improving CI/CD?
- Would it be valuable to have your CD solution use the same configuration and format as your CI, AND have visibility into the full product pipeline from idea to production?
- Tell me about the difficulties you're having managing complex pipelines and supporting integrations.
- We’ve also been having some really good conversations with customers that are in need of advice on how to start a discussion with their peers or managers around a potential change in how things are done today with CI. Is that something you’d be open to?

**Jenkins-Specific Questions**

- Are you using Jenkins for CI? Is it running critical/production jobs? 
  - Is one huge Jenkins master handling all of the load?
- Is your organization planning to continue investing in Jenkins over the next couple of years?
- Gitlab CI is Build as Code: Are all of your build configurations stored as code (Jenkinsfile) or is some of it stored in Jenkins itself?
- GitLab CI is HA / Mutable: Do you backup Jenkins? Is your Jenkins implementation setup with warm HA (if the server dies, it rebuilds itself)?
- Are you experiencing any regular issues with Jenkins?
- Who manages the GitLab / Jenkins integration (individual teams, or centralized)? 
  - If centralized, is there one single service account performing integration (git clone/pull actions)? - security concern
  - If individual teams, how much time do they spend configuring jobs and context switching across tools?

### Positioning 

- Competitive Assessments
- Demo guides and recordings


### CI/Verify Workshop

**Workshop Deliverables**

**Conversion**

Create a project plan for converting a project. Rough outline:

1. ​	Fork a project to GitLab.com and make it private.
2. ​	Add the customer and members of this team to the project.
3. ​	Convert the project to GitLab CI.Create a working .gitlab-ci.yml file.

**Enablement**

Resources:

[GitLab CI Enablement deck](https://docs.google.com/presentation/d/1eR_874yUHu5Yz8jC-7Gwtiz9j8N4APlgz7NT1_UR0mE/edit#slide=id.g849e6d84e3_0_636) (Gitlab internal WIP)

[Adopting GitLab CI at scale doc](https://docs.google.com/document/d/19oKupXi_nnFwD0VOilMhTH2nzUvrBN3P9hI-R5c6P8w/edit#heading=h.b61novry8f4t) (Gitlab internal WIP)

Key Topics:

1. Work with the customer’s users to enable them on GitLab CI/CD.
2. How to write a .gitlab-ci.yml file.
3. How to use the GitLab (Shared) Runners.
4. How to create templates for standardization & quick scaling

### Adoption

- Product documentation (Content owner: Product and Engineering Teams)
- Training assets
- Paid services
- Adoption Map

| Feature / Use Case | F/C  | Basic | S/P  | G/U  |
| ------------------ | ---- | ----- | ---- | ---- |
|                    |      |       |      |      |
|                    |      |       |      |      |
|                    |      |       |      |      |

The table includes free/community and paid tiers associated with GitLab's self-managed and cloud offering.

- F/C = Free / Core
- Basic = Bronze/Starter 
- S/P = Silver / Premium
- G/U = Gold / Ultimate

### Reporting and Metrics

- Link to telemetry metrics

### Learning Resources
- [CI Customer Use Case](/handbook/marketing/product-marketing/usecase-gtm/ci/#continuous-integration)
- [GitLab Journey and CI Use Case](https://www.youtube.com/watch?v=JoeaTYIH5lI&feature=youtu.be)
- [CS Skills Exchnage: CI Deep Dive](https://www.youtube.com/watch?v=ZVUbmVac-m8&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=3&t=0s)
- [CS Skills Exchange: Runners Overview](https://www.youtube.com/watch?v=JFMXe1nMopo&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=11&t=0s)
- [GitLab Runners Deep Dive](https://www.youtube.com/watch?v=GPSkNang9AU&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=19&t=0s)
- [Technically Competing Against Microsoft Azure DevOps](https://drive.google.com/open?id=18jwSeeUylGXv8LoEedCMRfBZt9t7QLOYKCHJp-SvdqA)
- [Competing Against Jenkins](https://drive.google.com/open?id=1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg)
- [DevOps Lifecycle: CI and CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
- Coming Soon: CI Learning Path 
