---
layout: markdown_page
title: "Gated Content"
---

## Overview
This page focuses on gated content as an offer for our marketing channels and integrated campaigns. The Content Marketing team focuses their quarterly and fiscal content plans ([see Content Marketing handbook page](/handbook/marketing/corporate-marketing/content/)) and Marketing Programs executes on the "gating" process - creating a landing page to generate leads - and activating within relevant channels and through existing and planned integrated campaigns. For an overview of the team and priorities, see the [Marketing Programs Management handbook page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/). If you have any questions please post in the [#marketing_programs Slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

#### Types of Content Programs
* Gated Content: We have the piece of content offered on our website via landing page.
    * Owned Content: We created the content in house
    * Analyst Content: We have bought the rights to use the content from an analyst such as Gartner, Forrester, etc.
* [Content Syndication](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication): We have promoted our content through a third-party, but do not direct back to our website. In these cases, we often have given them the resource to make available for download to their audience, and recieve the leads to be uploaded.

## Gated Content

#### Gating Criteria

**Gated:**
* Reports
* Whitepapers
* eBooks
* Guides
* On Demand Webcasts

**Not Gated:**
* Blogs
* Infographics
* Technical Training Resources
* Case Studies

## Timeline and Process Alignment

*With questions regarding this process, please comment in the [marketing_programs slack channel](https://gitlab.slack.com/messages/CCWUCP4MS)*

### `Internal Content` (created by the GitLab team)

Alignment to Campaigns: All gated content should be aligned to a content pillar (owned by Content Marketing) and a primary integrated campaign (owned by Marketing Programs). If no active or planned campaign is aligned, a campaign brief (including audience data) must be created for use by Digital Marketing Programs. _There should be a clear reason why we are moving forward with content that doesn't align to an integrated campaign._

**Please note that to take full advantage of all content produced, content may be used in multiple integrated campaigns and channels.**

To report clearly on integrated campaigns, each will have a landing page that offers content that is gated on its standard gated landing page url. The campaign landing page often uses different language aligned to the integrated campaign messaging.

####  Timeline for content launch & activation: (BD = business days)
* 📅 **T-30 BD** *Prep date:* The deadline for overall timeline to be agreed by all team members involved. All team members should aknowledge that they are aware of the content and agree to the timelines.
* 📝 **T-27 BD** *LP copy date:* The deadline for the landing page copy
* ✍️  **T-15 BD** *Final copy date:* The deadline for final copy (this is also when the final url will be provided to digital marketing programs)
* 🎨 **T-5 BD** *Content delivery date:* The deadline for the final design
* 📦 **T-3 BD** *Content load date:* The deadline for the asset to be added to a Pathfactory track and placed in a nurture stream
* 🚀 **T-0 BD** *Content launch date:* The date the gated content landing page will go live, and content is activated across channels

[See upcoming content & deadlines](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=1060299991&fvid=959545872)

#### Organizing content pillar epics and issues
1. **Content Pillar Epic:** `Content DRI` creates content pillar epic
1. **Content Asset Epics:** `Content DRI` creates content asset epics and associates to pillar epic
1. **Related Issues:** `Content DRI` creates the following issues and associates to the content asset epic that they relate to
   * [Copy Issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?issuable_template=design-request-content-resource) - name *[content-type copy]: [official name of content]* and assign to `Content DRI`
   * [Digital Design Issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?issuable_template=tbd) - name *[content-type promotion assets]: [official name of content]* and assign to `Content DRI` and `Design DRI`
   * [Design Issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?issuable_template=content-resource-request.html) - name *[content-type design]: [official name of content]* and assign to `Design DRI`
   * [Gating Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Request-MPM) - name *[content-type gating]: [official name of content]* and assign to `MPM DRI` and `Content DRI`
   * [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=mktg-promotion-template) *[content-type digital promotion]: [official name of content]* and assign to `Digital DRI` and `MPM DRI`

*Please note that if details and landing page copy are not provided, the MPM is blocked and will not be gating the resource.*

🏷️ **Label statuses:**
* **status:plan**: content is in a brainstormed status, not yet approved and no DRI assigned
* **status:wip**: content is approved, delivery date and launch date (5 days later) determined, DRI assigned

### `Analyst Content` (delivered by analysts)

Analyst reports (i.e. Forrester Wave, Garter Magic Quadrant, etc.) follow a different timeline with different steps to launch and activate. As soon as **Analyst Relations** is aware of an upcoming report and tentative launch timeline, an issue should be created, notifying Marketing Programs.

When **Analyst Relations** determines that they will be purchasing a report to be gated, they must [submit an issue using the analyst gating template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Request-Analysts-MPM).

If the analyst content is thought leadership (i.e. a whitepaper or non-competitor comparison report), it should be planned in advance of purchase with `at least a 30 business day time to launch date`. This allows time to plan activation into existing and future integrated campaigns and content pillars.

####  Timeline for content launch & activation: (BD = business days)
* 📅 **T-30 BD** *Prep date:* The deadline for overall timeline to be agreed by all team members involved. All team members should aknowledge that they are aware of the content and agree to the timelines.
* 📝 **T-27 BD** *LP copy date:* The deadline for the landing page copy
* 🚀 **T-0 BD** *Content load AND launch date:* The deadline for the asset to be added to a Pathfactory track and placed in a nurture stream. For analyst assets, the load date is also the date the gated content landing page will go live, and content is activated across channels.

[See upcoming analyst content & deadlines](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=1491223980)

#### Organizing analyst content epics and issues
1. **Analyst Report Plan Issue:** `Analyst Relations DRI` creates issue that outlines synopsis and tentative launch of upcoming report, in a plan state to determine if we will purchase.
1. **Gating Issue:** `Analyst Relations DRI` creates the [gating request issue template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Request-MPM) - name *[content-type gating]: [official name of content]* and assign to `MPM DRI` and `Analyst Relations DRI`
   * Analyst Relations DRI **must** fill out all information required in the first section of the issue
1. **Content Asset Epics:** `MPM DRI` creates asset epic *[report] Date/Quarter Name Exactly as Appears On Report* (ex. [report] 2018 Q3 Forrester New Wave: Value Stream Management (VSM) Tools)
1. **Related Issues:** `MPM DRI` creates the following issues and associates to the asset epic that they relate to
  * [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=mktg-promotion-template) - name *[content-type digital promotion]: [official name of content]* and assign to `Digital DRI` and `MPM DRI`
  * [Asset Expiration Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Expiration-Analysts-MPM)  - name *[Expire Asset]: [official name of content]* and assign to `MPM DRI` and `Content DRI`

*Please note that if details and landing page copy are not provided, the MPM is blocked and will not be gating the resource.*

🏷️ **Label statuses:**
* **status:plan**: content is in a brainstormed status, not yet approved and no DRI assigned
* **status:wip**: content is approved, delivery date and launch date (5 days later) determined, DRI assigned

## How to gate a piece of content on a standard gated content landing page

**If you are going to be uploading your gated asset as a PDF instead of PathFactory link**, follow the below steps first by creating a MR for the PDF before MR for landing page. *If you create the landing page MR before your PDF MR, you will get a 404 error when clicking the download link after the form fill.*
1. Upload your PDF to the [`resource/download`](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/resources) directory
    *  Ex: ebook-agile-delivery-models
2. Leave commit message including the asset type and asset name you are uploading
    *  In your MR, scroll down to the Changes tab and on your asset click the button `view on about@gitlab.com` and use that as your URL anywhere that you would be using a PathFactory link in the remaining steps (landing page code, Marketo token)
3. Follow the remaining steps for the Gated Content process

#### 1️⃣  Start with the Landing Page on about.gitlab
1. Navigate to the [/resources/ repo](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/resources)
1. Click the `+` dropdown and select `New Directory`
1. Add the directory name using the following syntax: `[type]-short-name`
   * *Name is very important as this is the url for the landing page!*
   * Keep it short, and use hyphens between words. (i.e. `ebook-ciso-secure-software`)
1. Add commit message to name your Merge request using syntax `Add [content type] landing page - [name of content]` (i.e. `Add ebook landing page - CISO Secure Software`)
1. Create a name for the target branch - NEVER leave it as the master (i.e. `jax-ebook-ciso`)
1. On the next screen (New Merge Request), add `WIP: ` to the beginning of the title and add a quick description (`Add LP for [content name]` will suffice)
1. Assign to Jackie Gragnola and scroll down, check the box for “Delete source branch when merge request is accepted”
1. Click `Submit Merge Request`
1. You’ve now created the merge request, but you need to add the file (code) for the landing page itself within the directory
1. Click on the link to the right of “Request to Merge” and you will enter the branch you’ve created with your MR
1. Click the `Source` folder, then `Resources` and click into the new directory as you named it
1. Click the `+` dropdown and select `New File`
1. Where it says “File Name” at the top, type in `index.html.haml`
1. Copy the following code: (see below)

```
---
layout: default
title: `add title here, don’t use colons`
suppress_header: true
extra_css:
  - styles-2018.css
destination_url: "`add the pathfactory url when available&lb_email="
form_id: "1002"
form_type: "resources"
cta_title: "Download the `type: eBook, etc`"
cta_date: 
cta_subtitle: 
link_text: "Click here to download the `type: eBook, etc`."
success_message: "You will also receive a copy of the `type: eBook, etc`sent to your inbox shortly."
---

.wrapper
  .page-illustrated-header-container
    = partial "includes/icons/gitlab-icon-pattern-header.svg"
    .container
      .header-container-content
        %h1.page-headline `add headline from copy doc`
        %h2.page-headline `add subhead from copy doc`

  .content-container
    .wrapper.container{ role: "main" }
      .row
        .col-xs-12.col-md-6.col-md-offset-1
          .content-section
            %p `Add P1 from copy doc`
            
            %p `Add P2 from copy doc`

            %h3 What you’ll learn in this `type: eBook, etc.`:
            %ul
              %li `add bullet 1`
              %li `add bullet 2`
              %li `add bullet 3`

            %p `add closing paragraph`

        .col-md-4.col-md-offset-1
          = partial "includes/form-to-resource", locals: { destination_url: current_page.data.destination_url, form_id: current_page.data.form_id, form_type: current_page.data.form_type, cta_title: current_page.data.cta_title, cta_date: current_page.data.cta_date, cta_subtitle: current_page.data.cta_subtitle, link_text: current_page.data.link_text, success_message: current_page.data.success_message }
```

1. Back in your MR, paste, the code where the file begins with 1.
1. Update the variables in the codeIn all the places in `snippet code`
  * *Be sure to remove the back-tick code symbols* so that the copy `does not turn red on the landing page`. This is just to be used as a guideline while editing the landing page.
1. If you don’t yet have a pathfactory link, leave the copy in that place. When you have  the Pathfactory link in a later step, you’ll edit the MR with the new link before pushing live (see later step)
1. Important! If there is a colon `:` in your title, replace with a dash `-` for the page title, as it will break the page.
1. In the Commit Message below your code, add a note that you’re adding the code for the landing page
1. Click `Commit Changes`
1. Now, while you wait for the pipeline to approve, move onto the next step in Marketo to facilitate the flows and tracking of the program.

#### 2️⃣  Create Marketo Program
1. Clone the [gated content template](https://page.gitlab.com/#PG2524A1) in Marketo
1. Choose clone to `A campaign folder`
1. Use naming syntax `[yyyy]_[type]_NameOfAsset` (i.e. 2019_eBook_CISOSecureSoftware) - keep this short
1. Add to folder: `Gated Content` > `Content Marketing`
1. Leave description blank and select “create”
1. When the new program loads, copy the url of the Marketo program and add to the description of the gating issue under `DRIs and Links`

#### 3️⃣  Create Salesforce Program
1. In the program summary, under “Settings” next to `Salesforce Campaign Sync` click on “not set”
1. In the popup that appear, click the down arrow and select `Create New`
1. Leave the name as it pre-fills so that it will exactly match the program name in Marketo
1. In the Description, write `Epic: ` and link to the content Epic url
1. Click save, and navigate to the [campaigns view](https://gitlab.com/groups/gitlab-com/marketing/-/epics/400) in Salesforce
1. Choose the “Gated Content” campaign view and make sure it is in order by Created Date (first column) from latest to earliest creation. Your campaign should appear at the top.
1. Click into your campaign and change the owner to your name
1. Copy the url of the Salesforce campaign and add to the description of the gating issue under `DRIs and Links`

#### 4️⃣  Update the Marketo Tokens
1. Navigate to the `My Tokens` section of the Marketo program, and make the following edits:
  * Content Download URL: add the PathFactory link after the asset is loaded into Pathfactory and added to a track. This is provided by the Pathfactory DRI. You may need to update the confirmation email button link to follow [PathFactory link criteria](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/pathfactory-management/#appending-utms-to-pathfactory-links) or to remove extra tokens that will break the PDF token.
  * Content Epic URL: the url to the overall epic of the content
  * Content Title: the content title as it appears on the asset - not necessarily what is displayed as the landing page header on the copy doc (i.e. `10 Steps Every CISO Should Take to Secure Next-Gen Software`)
  * Content Type: select the appropriate content type (i.e. leave just `eBook`)
  * UTM: leave the code that is originally there, and at the end of the code, add the name of the Marketo program, removing any underscores or special characters (i.e. `2019eBookCISOSecureSoftware`)
  * Copy and paste the 3 bullets from the content copy document into the 3 tokens aligned to the bullets

#### 5️⃣  Preview the emails
1. Preview the confirmation email to make sure that the tokens appear correctly.
1. If you haven’t uploaded to Pathfactory and received a final link, your button will be broken. When you add that to the Marketo tokens, the button will work properly.

#### 6️⃣  Edit, Review, and Activate Marketo Smart Campaign
1. In the `01 Downloaded Content` smart campaign, under `Smart List`add the url created in step 1 to the `/resources/` link in the referrer constraint (i.e. add `ebook-ciso-secure-software` after`/resources/` so that it is `/resources/ebook-ciso-secure-software`
  * In the `Flow` section, review the steps. It should include:
  * Send Email: Confirmation email from your program (this automatically pulls in)
  * Remove from flow with “choice” that if email address contains `@gitlab.com` it should not continue the steps
  * Change program status in your program to `Gated Content  > Downloaded`
  * Interesting moment using tokens with “Web” type, and description: `Downloaded the {{my.content type}}: “{{my.Content Title}}`
  * Send alert to `None`and {{my.mpm owner email address}} (reps receive MQL alerts, so this only triggers to the MPM and can be filtered out of your email if desired).
  * Change data value if `Acquisition Program` is empty to now be the current program
  * Change data value if `Person Source` is empty to be “Gated Content - {{my.content type}}”
  * Change data value if `Initial Source` is empty to be “Gated Content - {{my.content type}}”
1. In the Schedule section, check that it’s set for each person to run through the flow every 7 days and click `Activate`

#### 7️⃣  Test Your Page in the Review App
1. Navigate back to your MR in GitLab
1. When your pipeline has complete, click “View App” and add the URL you chose for your page (i.e. add `/resources/ebook-ciso-secure-software`to the end of the view app url
1. Fill out the form using your test lead (using email syntax [your email]+[name]@gitlab.com - i.e. `jgragnola+george@gitlab`)
1. Check in the Marketo program that it triggered the autoresponder, addition to the program, and alert.
1. In your inbox, check the confirmation copy and urls.
1. If this fires properly, return to the MR and assign to Jackie with a comment to ping her and let her know it’s ready to merge.
1. Check off all completed actions on the issue.
1. When the MR merges into the master, it will appear at the URL you determined.
1. When it has appeared, test it once more with a different test lead email address, and confirm that everything fires as desired.

#### 8️⃣ Add Your Page to the Rsesources Page
1. Begin a new MR from [the resources yml](https://gitlab.com/gitlab-com/www-gitlab-com/edit/master/data/resources.yml)
2. Use the code below to add a new entry with the relevant variables
3. Add a commit message, rename your target branch, leave "start a new merge request with these changes" and click "Commit Changes"
4. Update the name of your merge request to be `Add [resource name] to Resource Center`
5. Assign the merge request to yourself
6. When you've tested the MR in the review app and all looks correct (remember to test the filtering!), assign to Jackie
7. Comment to Jackie that the MR is ready to merge

**Code:**
```
- title: 'Add name of resource - shorten if necessary'
  url: 
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - 
    - 
  solutions:
    - 
    - 
  teaser: 'Add a teaser that relates to the contents of the resource'
```

**Example:**
```
- title: '10 Steps Every CISO Should Take to Secure Next-Gen Software'
  url: /resources/ebook-ciso-secure-software/
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - DevSecOps
    - Security
  solutions:
    - Security and quality
  teaser: 'Learn the three shifts of next-gen software and how they impact security.'
```

**IMAGES to choose from (select one):**
*[Shortcuts to Images Folder](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/resources)
* `/images/resources/cloud-native.png`
* `/images/resources/code-review.png`
* `/images/resources/continuous-integration.png`
* `/images/resources/devops.png`
* `/images/resources/git.png`
* `/images/resources/gitlab.png`
* `/images/resources/security.png`
* `/images/resources/software-development.png`

**TOPICS to choose from (add all that apply):**
* Agile
* CD
* CI
* Cloud Native
* DevOps
* DevSecOps
* Git
* GitLab
* Public Sector
* Security
* Single Applicaton
* Software Development
* Toolchain

**SOLUTIONS to choose from (add all that apply):**
* Accelerated delivery
* Executive visibility
* Project compliance
* Security and quality

#### 🏁  Notify the Team
1. Comment in the issue with the final URL and a message that the page is now live and working as intended.
1. Close out the issue.
 

## Content Syndication

*This section has been moved to a dedicated handbook page: [Content Syndication](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication)