---
layout: markdown_page
title: "Paid Surveys"
---

## Overview
This page focuses on paid surveys through external vendors as a tactic used within marketing campaigns, including account centric campaigns.

### Process in GitLab to organize epic & issues

The FMM or requester is responsible for following the steps below to create the epic and related issues in GitLab.

1. FMM creates the main tactic issue
1. FMM creates the epic to house all related issues (code below)
1. FMM creates the relevant issues required (shortcut links in epic code below)
1. FMM associates all the relevant issues to the newly created epic, as well as the original issue
1. FMM sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of a survey changes, the FMM is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

**Name: Paid Survey - [Vendor] - [3-letter Month] [Date], [Year]**

```
<--- Name this epic using the following format, then delete this line: Paid Survey - [Vendor] - [3-letter Month] [Date], [Year] --->

## [Main Issue >>]()

## [Copy for landing page and emails >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **FMM/Requester:** 
* **MPM:**  
* **FMC:** 
* **Type:** Paid Survey
* **Account Centric?** Yes/No
* **Budget:** 
* **Launch Date:**  [MM-DD-YYYY] (this is the date the survey begins through the vendor)
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] campaign utm `` - FMM to fill in based on budget doc

## User Experience
[What is the anticipated user experience? FMM to provide a description of the journey as if they were the end user - from communications received (both from GitLab and/or from vendor), to what the user provides in survey, what happens after they submit, what do they receive from us after we upload the leads, and beyond... what is the end-to-end journey for the user?]

## Additional description and notes about the tactic
[FMM add whatever additional notes are relevant here]

## Issue creation

* [ ] [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - FMM creates, assign to MPM
* [ ] [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - FMM creates, assign to FMM, MPM and FMC
* [ ] [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - FMM creates, assign to FMM and MPM
* [ ] [Gated content request issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Request-MPM) (*optional - only if we have rights to survey results and content is worth gating*) - FMM creates, assign to FMM, MPM and FMC

/label ~"Marketing Programs" ~"Field Marketing" ~mktg-status:plan ~"Paid Survey"
```