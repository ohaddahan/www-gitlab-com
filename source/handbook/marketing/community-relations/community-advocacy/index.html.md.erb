---
layout: handbook-page-toc
title: "Community Advocacy"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who we are

<%= direct_team(manager_role: 'Sr. Community Advocates Manager') %>

## Finding the Community Advocates

- [**Community Advocacy Issue Tracker**](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/issues); please use confidential issues for topics that should only be visible to team members at GitLab.
- [**Chat channel**](https://gitlab.slack.com/messages/community-relations); please use the `#community-advocates` chat channel for questions that don't seem appropriate to use the issue tracker for.

### Emergency contact

- [**Advocates contact**](https://docs.google.com/document/d/1rjEKcIUC2H1HmPuQyijllVu044RxaEnRXH3GQKCsKyI/edit#heading=h.r01iolr373k3) 
- [**Incident management roles**](/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) 
- **Relevant Slack channels for incident/infrastructure:** [`#incident-management`](https://gitlab.slack.com/messages/incident-management), [`#infrastructure-lounge`](https://gitlab.slack.com/messages/incident-management)

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Community Advocate Resources

- Community Advocate Bootcamp
  - [Bootcamp](/handbook/marketing/community-relations/community-advocacy/onboarding/bootcamp/)

----

## Role of Community Advocacy

### Goal

The goal of community advocacy is to respond to all of the GitLab mentions and questions asked online in a timely manner. 

### Plan

1. Have discount codes that are easily distributed by team members
1. Send every major contributor a personalized gift
1. Expand the coverage of all GitLab mentions.
1. Improve responsiveness on high priority channels.
1. Do the rest of the [contributor journey](/handbook/journeys/#contributor-journey)

### Vision

1. GitLab has 1000's of active content contributors (e.g. for blogs, meetups, presentations, etc.)
1. Being a core contributor is a very rewarding experience
1. There are 10's of active GitLab/[ConvDev](http://conversationaldevelopment.com/) meet-ups
1. 100's of talks per year given at conferences and meetups
1. Our most active content contributors come to our summits
1. 100's of people contribute content about GitLab every month
1. We use software that helps us to keep track of core contributors (can be forum, Highrise, software made for advocacy, or a custom Rails app)
1. There is a core contributors page organized per region with the same information as the [team page](/company/team/) and what they contributed, where they work (if they have a LinkedIn profile), and a button to sent them an email via a form.
1. We measure and optimize every step of the [contributor journey](/handbook/journeys/#contributor-journey)

### Respond to every community question about GitLab asked online

- This includes helping members of the community with _their_ questions, but also making sure that the community is heard and that the feedback from the community reaches the rest of the team at GitLab.
- Engage with the developer community in a way that is direct but friendly and authentic. Be able to carry the tone of the GitLab brand while also giving the proper answers or direction to members of the community.
- [Engage with experts in the GitLab team](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts) to provide the best quality answers and to expose them to community feedback.
- Help update the [social media guidelines](/handbook/marketing/social-media-guidelines/) and GitLab voice as new situations arise.
- Explore different tools from Zendesk to Mentions to find a way to track all mentions of GitLab across the internet.
- Don’t be afraid of animated gifs and well-placed humor! We are not robots.
- Work within the GitLab process to help users report bugs, make feature requests, contact support, and provide feedback on the product.

### Respond to every question asked internally

- This includes helping GitLab team members with _their_ questions across the `#community-advocates`, `#community-relations`, `#education-oss`, `#swag` Slack channels and making sure that they are heard, and that we help them with our input and assistance.
- Make sure to enable the Slack notifications for all the new messages in the channels mentioned above
- Help the team with tailoring and reviewing the drafts of their responses going online.

## Involve experts

As Community Advocates, we will often want to involve experts in a topic being discussed online. The [Involving experts workflow section](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts) describes how we do it.

### Can you please respond to this?

You got a link to this because we'd like you to respond to the mentioned community comment. We want to make sure we give the best answer possible by connecting the wider community with our experts and expose you to more community feedback.

#### Expert Response Guidelines

* When responding to community mentions, you should check out the [social media guidelines](/handbook/marketing/social-media-guidelines/).
* Since the community members can't participate in the internal Slack discussions, please answer in the social channel that the comment was originally posted. This builds community directly between GitLab team members and the wider community. 
* If you can't respond to the linked comment, that's OK, but please quickly let the person who pinged you know so they can ping someone else. Just sharing a project or an issue your team has on the horizon can also be an effective response.
* Sometimes, a simple 'thanks' or recognition is enough.
* We value efficient communication, but this could come across as short to the wider community. Think about tone and authenticity when responding.

#### Expert Response Steps

* Consider the best [response strategy](/handbook/marketing/community-relations/community-advocacy/#expert-response-strategies) and navigate to the comment provided by the Advocate
* Respond on the related social channel using your personal account
   - When responding on [news.ycombinator.com](https://news.ycombinator.com/), please make sure the `about` field on your account page is representing your current role at GitLab
   - When responding on our website comments, please use your personal Disqus account
   - We can upgrade your [GitLab forum](http://forum.gitlab.com/) account to Admin if needed
* Let the Advocate know that you responded on Slack, so they can close the related ticket on Slack
* No further action is required from your side - advocates will notify you again if needed

#### Expert Response Strategies

If you or your team is unsure how to best collaborate on a community response, consider using one of these strategies:

* Discuss potential responses on the Slack thread where a Community Advocate has pinged you. Advocates can jump in and help with the response.
* Create a collaborative Google Doc to draft and plan response with team members asynchronously. See [this example](https://docs.google.com/document/d/1GGO2kef7uTHxrBYVi4lUo8fbiIThggv1OfLLAtqPzzU/edit) from experts on the Manage team whose collaboration resulted in a [personal and detailed Twitter response](https://twitter.com/iamyoginth/status/1204293292893949952).
* Learn more about building community by attending the Community Relations monthly Book Club or reading about our discussion in [this issue](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/issues/63)
* If you're part of the customer success team, consider adding your social responses to this [shared snippet page for social responses](https://gitlab.com/guided-explorations/shared-snippets/snippets)

## Community response channels

The Community Advocates actively monitor and respond to the following set of channels.

In this overview:
- Those channels not marked as active need a response workflow to be put in place and are currently monitored on an occasional basis.
- Each channel has a link to the workflow in place to process responses

| CHANNEL | SOURCE | AUTOMATION | DESTINATION | ACTIVE? |
| - | - | - | - | - |
| [`@gitlab`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter) | Twitter mentions | Zendesk | Zendesk | ✓ |
| [`@movingtogitlab`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter)  | Twitter mentions | Zendesk | Tweetdeck | ✓ |
| [`@gitlabstatus`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter)  | Twitter mentions | Zendesk | Zendesk | ✓ |
| [Facebook](/handbook/marketing/community-relations/community-advocacy/workflows/facebook)  | Facebook page messages | Zapier | Zendesk | ✓ |
| [Hacker News](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews) | Hacker News mentions | Zapier | Zendesk and Slack: #hn-mentions | ✓ |
| [Hacker News front page stories](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews) | Hacker News front page mentions | Zapier | Slack: #community-advocates | ✓ |
| [Education Program](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup) | Education application form | Marketo | Salesforce and Zendesk | ✓ |
| [Open Source Program](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup) | Open Source application form | Marketo | Salesforce and Zendesk | ✓ |
| [Startups Program](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup) | Startup application form | Marketo | Salesforce and Zendesk | ✓ |
| [E-mail (merch@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | Shop contact | E-mail alias | Zendesk | ✓ |
| [E-mail (community@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | Handbook | E-mail alias | Zendesk | ✓ |
| [E-mail (movingtogitlab@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | #movingtogitlab campaign (deprecated) | E-mail alias | Zendesk | ✓ |
| [E-mail (education@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (opensource@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (startups@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (personal inbox)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | E-mails to track as tickets | E-mail alias | Zendesk | ✓ |
| [Website: blog](/handbook/marketing/community-relations/community-advocacy/workflows/website-comments) | Disqus comments | Zapier | Zendesk, #mentions-of-gitlab | ✓ |
| [Website: DevOps Tools](/handbook/marketing/community-relations/community-advocacy/workflows/devops-tools) | Disqus comments | Zapier | Zendesk and Slack: #devops-tools-comments | ✓ |
| [Speakers](/events/find-a-speaker/) | Find-a-speaker form | Zapier | Zendesk | ✓ |
| [Reddit](/handbook/marketing/community-relations/community-advocacy/workflows/reddit)  | Reddit mentions | Zapier | Zendesk and Slack: #reddit | ✓ |
| [Documentation](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | Disqus comments | Zapier | Slack: #docs-comments | ✓ (Docs Team)|
| [Stack Overflow](/handbook/marketing/community-relations/community-advocacy/workflows/stackoverflow) | Stack Exchange mentions | Zapier | Zendesk | ✓ |
| [GitLab forum](/handbook/marketing/community-relations/community-advocacy/workflows/forum) | forum.gitlab.com | Zapier | Zendesk and Slack: #gitlab-forum | ✓ |
| [Lobste.rs](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | lobste.rs mentions | Zapier | Slack: #mentions-of-gitlab | ✖ |
| [IRC](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | IRC support | N/A | N/A | ✖ |
| [Gitter](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | Gitter support | N/A | N/A | ✖ |
| [YouTube](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | YouTube comments | N/A | N/A | ✖ |
| [Mailing list](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | GitLabHq Google Group (deprecated) | N/A | N/A | ✖ |
| [Quora](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | GitLab Quora topic | N/A | N/A | ✖ |
| [Wider community content](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | Blog post comments | N/A | N/A | ✖ |

## How we work

- [Community advocacy workflows](/handbook/marketing/community-relations/community-advocacy/workflows/)
- [Community advocacy guidelines](/handbook/marketing/community-relations/community-advocacy/guidelines/)

## <i class="fas fa-calendar-check fa-fw color-orange font-awesome" aria-hidden="true"></i> Coverage for important and/or urgent mentions

At this time, the Community Advocates team spans across two main timezones: CET (Central European Time, UTC +1) and CDT (Central Daylight Time, UTC -5). Our typical coverage based on these time zones is Monday - Friday from 8:00UTC to 22:00UTC, plus occasional [weekend coverage for release days](/handbook/marketing/community-relations/community-advocacy/#release-day-advocate-duty). 

While this gives us the capacity to address most mentions during the working day, often [important and/or urgent mentions](/handbook/marketing/community-relations/community-advocacy/guidelines/general/#urgent-and-important-mentions) happen outside our current coverage times. An example is HackerNews mentions happening towards the end of business hours or later in the Pacific Timezone (UTC-8).

### With the full team online

This is the ideal case where there is coverage from the full team and we follow the regular workflows for each one of our [monitored channels](/handbook/marketing/community-relations/community-advocacy/#community-response-channels).

**Community Advocates Coverage Handoff**: before ending your day, please review ongoing conversations and mentions in our channels to assess whether there is any one of them that could potentially become important and/or urgent. If that is the case,

- [Involve the relevant experts](/handbook/marketing/community-relations/community-advocacy/#involving-experts) as you would generally do. Let them know you are going offline at the end of your workday and ask them to monitor the channel or find someone on their team who can do it while you're out.
- Assign corresponding Zendesk tickets to the group of advocates who will be online next. In Zendesk, these groups are named `APAC`, `EMEA`, and `AMER` and contain each advocate working in those specific timezones. Mark the priority of the ticket as either `high` or `urgent`. Be sure the Zendesk ticket links to all relevant discussions/expert outreach on Slack.
- If a Zendesk ticket is marked as `urgent`, you should also ping `@advocates` in the [#community_advocates](https://gitlab.slack.com/messages/community-advocates) slack channel for higher visibility.
- When your coverage begins each day, first check Zendesk for any tickets assigned to your group. Process tickets marked `urgent` first, and `high` second. After you've reviewed and taken necessary action on these tickets, start your regular processing workflow.


### With a team member offline

**Community Advocates**: if a member of the Community Advocates team is offline during their regular working hours (e.g. due to Paid Time Off, illness or unforeseen events) for a day or more, and that leaves their timezone uncovered, please activate the [Advocate for a day process](/handbook/marketing/community-relations/community-advocacy/#advocate-for-a-day) with at least two additional advocates.

If the time offline extends more than a few days, it is advisable to find additional advocates and rotate their roles.

### After hours

In general, if you notice an online mention that needs to be addressed, please ping `@advocates` on the [#community-advocates](https://gitlab.slack.com/messages/community-advocates) Slack channel. All [Community Advocates have notifications enabled for this group handle and this channel](/handbook/marketing/community-relations/community-advocacy/#respond-to-every-question-asked-internally) –notifications are also sent if the handle is mentioned on any GitLab Slack channel.

You can also [contact any of the Community Advocates] directly via Slack or text message.

If required, please consider using the [Marketing Rapid Response Process](/handbook/marketing/#-marketing-rapid-response-process) as well.


## Deliverable scheduling

* Team meetings are on Tuesday
* One on one's are on Thursday
* All deliverables are expected to be completed by Friday of the running week
  * Lots of time to complete
  * Enough time to review
  * Enough time for resolving potential problems
  * Small deliverables force small/fast iterations
* All deliverables are expected to be merged by Tuesday
* For every handbook update (that substantially changes the content or layout), please follow the [handbook guidelines](/handbook/handbook-usage/#handbook-guidelines)

## Release day advocate duty

Every 22nd of the month we release a new version of GitLab. More often than not we get a spike in community mentions. To help deal with this we have dedicated release advocates that own the effort of responding to community mentions on/after a release.

Every month a different advocate has release advocate duty. It rotates on a monthly basis. If the release day takes place on a weekend, one of the advocates is assigned to monitor the traffic and to process mentions. We keep track of the assignments on the `Community Advocates` GitLab team calendar.

The two channels that we see the biggest increases in are:

* [The GitLab blog](/blog/)
* [HackerNews](https://news.ycombinator.com/news)

### Release day tasks

- Consider engaging with an [Advocate-for-a-day](#advocate-for-a-day) in advance
- Monitor the `#release-post` Slack channel throughout the day to be ready at the time the release blog post is published
- Copy the overview of the three main features or improvements from the beginning of the release blog post. Post this overview on the relevant social channels (HackerNews will be the main one to post it to). You can use the [11.8 summary post](https://news.ycombinator.com/item?id=19228781) as an example.

## Additional responsibilities

### Support for Education, Open Source and Startup Programs

Community Advocates support the [Education](/handbook/marketing/community-relations/education-program), [Open Source](/handbook/marketing/community-relations/opensource-program) and [Startups](/solutions/startups) Programs, and their Program Managers.

Their role is to process and manage program applications as per the [application management workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup).

### Contributor recognition

## Initiatives

[Learn more about recognizing contributors](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/community-rewards-internal)

### Merchandise

Community Advocates manage merchandise requests, inventory and the merchandise tech stack.

[Learn more about how Community Advocates manage merchandise](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling)

### #movingtogitlab

During news cycles such as the [Microsoft acquisition of GitHub](https://news.microsoft.com/2018/06/04/microsoft-to-acquire-github-for-7-5-billion/), there may be an increase in new GitLab users. The movingtogitlab specific [Twitter account](https://twitter.com/movingtogitlab) highlights all the users who tweeted about switching to GitLab.

Advocates should always look for new users moving to GitLab and make sure to thank them and ask what the benefits of using our products are. When doing so, please follow the [#movingtogitlab workflow](/handbook/marketing/community-relations/community-advocacy/workflows/moving-to-gitlab).

## Advocate for a Day

When community advocates aren't available, or we expect high traffic on social media (because of some major outage, or some significant announcement), we should try to recruit more GitLab team-members who would help us cover our social networks. Our [Advocate for a Day](/handbook/marketing/community-relations/community-advocacy/workflows/advocate-for-a-day) page is meant to help assist anyone who has been asked to perform this duty.

## Expertises

Every Community Advocate owns one or more of the team's processes. These are called expertises.

- Merchandise - [Samantha](https://gitlab.com/slee24)
- [Support for Education, Open Source and Startup Programs](#support-for-Education-open-source-and-startup-programs) - [Borivoje](https://gitlab.com/borivoje)
- Zendesk integrations - [Emily](https://gitlab.com/ecook1)
- GitLab Forum - [Lindsay](https://gitlab.com/LindsayOlson)

### Expertise rotation

From time to time we run [expertise rotation](/handbook/marketing/community-relations/community-advocacy/workflows/expertise-rotation) rounds to ensure all Advocates are trained in all expertises.

## Tech stack

### Customers portal
The [Customers Portal](https://customers.gitlab.com/) is the in-house platform to manage customer subscriptions. We use it mostly in the context of the Education, OSS and startup programs for:
  - Tracking, editing and resending EULAs
  - Changing the Primary contacts on existing subscriptions
  - Manually upgrading user's GitLab.com groups

[Learn more about the Customers Portal](/handbook/internal-docs/customers-admin/).

### Discourse

[Discourse](https://www.discourse.org) is the platform on which the [GitLab forum](https://forum.gitlab.com) is run.

[Learn more about how we use Discourse](/handbook/marketing/community-relations/community-advocacy/workflows/forum/#administration).

### Disqus

[Disqus](https://disqus.com) is the commenting platform we use to enable our wider community to comment on the [GitLab website](/handbook/marketing/community-relations/community-advocacy/workflows/website-comments), the [DevOps Tools pages](/handbook/marketing/community-relations/community-advocacy/workflows/devops-tools), and documentation comments.

[Learn more about how we use Disqus](/handbook/marketing/community-relations/community-advocacy/tools/disqus)

### License App
The [License app](https://license.gitlab.com) is the in-house platform to generate licenses. Access to the LicenseApp [requires a dev account](/handbook/business-ops/it-ops-team/access-requests/#i-need-access-to-versiongitlabcom-or-licensegitlabcom). In the context of the Education, OSS and startup programs, the License App:
- Can be used for downloading and resending license keys for the self-hosted subscriptions
- It can also be used for generating trial licenses. Always make sure to document it in the SFDC as a comment on the appropriate account

### Printfection

[Printfection](https://www.printfection.com/) is the swag management platform. Community Advocates use it to manage the inventory and orders for most swag items.

[Learn more about how Community Advocates manage merchandise](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling).

### Salesforce

[Salesforce](https://www.salesforce.com) is the [CRM](https://en.wikipedia.org/wiki/Customer_relationship_management) platform we use to [support the Education, Open Source and Startup Programs](#support-for-Education-open-source-and-startup-programs).

### Shopify

[Shopify](https://www.shopify.com) is the [GitLab store](https://shop.gitlab.com) frontend platform. Community Advocates use it to publish merchandise items for sale, for reports and inventory tracking.

[Learn more about how Community Advocates manage merchandise](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling).

### Stickermule

[Stickermule](https://www.stickermule.com/) is the swag platform used to print and order custom stickers.

[Learn more about how Community Advocates manage merchandise](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling).

### TanukiDesk

[TanukiDesk](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/tanukidesk) is an in-house open source project. Its goal is to provide bidirectional communication between Zendesk and Disqus/HackerNews.

We're currently utilizing this app to work on Disqus mentions via [TanukiDesk Disqus Channel](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/tanukidesk_disqus_app) app.

### Tweetdeck

[Tweetdeck](https://tweetdeck.twitter.com/) is used in special cases to post and manage Twitter responses. Otherwise, Twitter responses are tracked and most generally posted directly from [Zendesk](#zendesk). 

[Learn more about how we use Tweetdeck](/handbook/marketing/community-relations/community-advocacy/workflows/twitter).

### Zapier

[Zapier](https://zapier.com) is an automation tool used to identify mentions across [our response channels](#community-response-channels) and to route them into [Zendesk](#zendesk) as tickets

[Learn more about how we use Zapier](/handbook/marketing/community-relations/community-advocacy/tools/zapier).

### Zendesk

[Zendesk](https://www.zendesk.com/support/) is the central place for Community Advocates to track wider community mentions across [our response channels](#community-response-channels) and to effectively respond to them. It also used to measure our [KPI](#kpis)s.

[Learn more about how we use Zendesk](/handbook/marketing/community-relations/community-advocacy/tools/zendesk)

## Advocate and Social Team Shadow Program

Purpose: The purpose of this shadow program is to build effective collaboration and community building strategies between the Community Advocates and the Social Media team.

Outcomes:
1. The Advocates will gain an increased understanding of the [Social Marketing Handbook](/handbook/marketing/corporate-marketing/social-marketing/) and learn new strategies for engaging with community members on social channels to promote GitLab brand awareness.
2. The Social Team will gain an increased understanding of the [Community advocate workflows](/handbook/marketing/community-relations/community-advocacy/workflows/) and how the Advocates reroute or respond to different types of GitLab mentions. 
3. Following the shadow sessions, the Advocates will build guidelines in the handbook to better define paths of action for response for various types of GitLab mentions.

The planning of this process can be followed on [this GitLab issue](https://gitlab.com/gitlab-com/marketing/community-relations/general/issues/12).


## KPIs

### Community Channel Response Time [KPI](/handbook/ceo/kpis/) Definition
Community Channel Response Time is the time between an inbound message and the first-reply time.
The current goal is to be under 7 hours for all channels and under 5 hours for high-priority channels.
This response time is currently tracked in Zendesk.

### Metrics Update Process
Each month the marketing team hosts a Key Monthly Marketing Metics meeting. The advocates team is responsible for updating relevant data in the presentation slides.

* All advocates should join the [#keymonthlymarketingmeeting](https://app.slack.com/client/T02592416/CM5UC5XPT) Slack channel. Updates are coordinated here and all channel members will be pinged when the slides are ready for updates.

#### Adding Metrics to Slides
* In Zendesk, navigate to Reporting -> Insights Tab -> Overview
* Take a screenshot of the 'Median First Reply Time'
* Take a screenshot of the 'Daily ticket activity for the last 30 days'
* Add both screenshots to the Channel Response Time presentation slide.
* Report on [#keymonthlymarketingmeeting](https://app.slack.com/client/T02592416/CM5UC5XPT) Slack channel that the slide is up to date

### Quarterly OKR Epic Creation Process
Each quarter we define and have the opportunity to [share our OKRs publicly](https://about.gitlab.com/company/okrs). An advocate is responsible for creating an epic from the content in the quarter's [OKR page](https://about.gitlab.com/company/okrs/fy21-q1/) by following the steps listed below. 

Translating our OKRs into an epic each quarter is in line with our transparency value, and it helps us collaborate better as a team when it comes to our objectives and key results.

#### Steps to Create a New Epic for Community Advocates Quarterly OKRs
* Open the relevant quarter's [OKR handbook page](https://about.gitlab.com/company/okrs/fy21-q1/)
* Search for "Director of Community Relations" in the page (in your browser, try using command-f or Ctrl-f to quickly find the phrase)
* Scroll through the OKRs assigned to "Director of Community Relations" until you find the associated Community Advocate OKR/KRs 
* Copy the list of bulleted KRs you find
* [Navigate to this list of the Community Relations Team's epics with an OKR tag](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics?label_name=OKR) - you will be creating a new epic here with the list of KRs you copied from the handbook page
* Click the `New Epic` button in the upper right corner and follow the title naming convention "FY2X-QX OKR: *enter name of OKR* " (ex. FY21-Q1 OKR: Scale Community Advocates team)
* Edit your new Epic to include the bulleted list of KRs you copied
* Add the red `OKR` tag
* Add a due date (usually the last day of the quarter)
* Save changes, and tag teammates if you need support in the comments of the epic
* [Click here to see an example of a Community Advocate Quarterly OKR Epic](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/15)

## Relevant Links

- [Social Media Guidelines](/handbook/marketing/social-media-guidelines/)
- [Support handbook](/handbook/support/)
